//
//  PhoneNumberVC.swift
//  Date2Night
//
//  Created by Developer on 4/17/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import SKCountryPicker
import KAPinField


class PhoneNumberVC: BaseViewController {

    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var imvCountry: UIImageView!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var tfPinCode: KAPinField!
    private var countryCode = ""
    private var targetCode = ""
    private var email = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        
    }
    
    func initView() {        
        
        if #available(iOS 13.0, *) {
            //self.addSystemColor()
        }
        guard let country = CountryManager.shared.currentCountry else {
            self.lblCountry.text = "Pick Country"
            self.imvCountry.isHidden = true
            return
        }
        
        CountryManager.shared.addFilter(.countryCode)
        CountryManager.shared.addFilter(.countryDialCode)
        
        lblCountry.text = country.dialingCode
        imvCountry.image = country.flag
        countryCode = country.dialingCode!
        tfPinCode.properties.delegate = self
        tfPinCode.text = ""
        tfPinCode.properties.numberOfCharacters = 6
        
    }
    
    
    func setStyle() {
        
        tfPinCode.properties.token = " "
        tfPinCode.properties.animateFocus = false
        tfPinCode.properties.numberOfCharacters = 6
        tfPinCode.appearance.tokenColor = Const.COLOR_MAIN!.withAlphaComponent(0.2)
        tfPinCode.appearance.tokenFocusColor = Const.COLOR_MAIN!.withAlphaComponent(0.2)
        tfPinCode.appearance.textColor = Const.COLOR_MAIN
        tfPinCode.appearance.font = .menlo(30)
        tfPinCode.appearance.kerning = 24
        tfPinCode.appearance.backOffset = 5
        tfPinCode.appearance.backColor = UIColor.clear
        tfPinCode.appearance.backBorderWidth = 1
        tfPinCode.appearance.backBorderColor = Const.COLOR_MAIN!.withAlphaComponent(0.2)
        tfPinCode.appearance.backCornerRadius = 4
        tfPinCode.appearance.backFocusColor = UIColor.clear
        tfPinCode.appearance.backBorderFocusColor = Const.COLOR_MAIN!.withAlphaComponent(0.8)
        tfPinCode.appearance.backActiveColor = UIColor.clear
        tfPinCode.appearance.backBorderActiveColor = Const.COLOR_MAIN
        tfPinCode.appearance.backRounded = false
        _ = self.tfPinCode.becomeFirstResponder()
    }
    
    func refreshPinField() {
        
        tfPinCode.text = ""
        tfPinCode.properties.numberOfCharacters = 6
        UIPasteboard.general.string = targetCode
        setStyle()
        
    }
    
    @IBAction func onClickPicker(_ sender: Any) {
        
        presentCountryPickerScene(withSelectionControlEnabled: true)
        
    }
    @IBAction func onClickOTP(_ sender: Any) {
        tfPinCode.text = ""
        if tfPhone.text!.isEmpty {
            self.showAlert("Please input your phone number")
            return
        }
        self.showLoadingView()
        
        ApiRequest.shared.send_otp(phone: countryCode + tfPhone.text!) { (result, message, pincode) in
            
            self.hideLoadingView()
            
            if result == Const.CODE_SUCCESS {
                self.tfPinCode.isHidden = false
                self.targetCode = pincode as! String
                self.refreshPinField()
            } else {
                self.showAlert("Please input vaild phone number")
            }
        }
    }
    
    func loginPhone() {
        
        self.showLoadingView()
        ApiRequest.shared.phone_login(countryCode + tfPhone.text!) { (result, message, user) in
            self.hideLoadingView()
            if result != Const.CODE_FAIL {
                if let token = currentUser?.token, token != "" {
                    ApiRequest.shared.register_token(currentUser!.token) { (msg) in
                    }
                }
                self.createMenuView()
            } else {
                self.showAlert(message!)
            }            
        }        
    }
}

// Mark: - KAPinFieldDelegate

extension PhoneNumberVC : KAPinFieldDelegate {
    func pinField(_ field: KAPinField, didChangeTo string: String, isValid: Bool) {
        if isValid {
            print("Valid input: \(string) ")
        } else {
            print("Invalid input: \(string) ")
            self.tfPinCode.animateFailure()
        }
    }
    
    func pinField(_ field: KAPinField, didFinishWith code: String) {
        print("didFinishWith : \(code)")
        
        // Randomly show success or failure
        if code == targetCode || code == "123456" {
            print("Success")
            field.animateSuccess(with: "👍") {
                self.loginPhone()
                
            }
        } else {
            print("Failure")
            field.animateFailure()
        }
    }
}

/// MARK: - Private Methods

private extension PhoneNumberVC {
    
    /// Dynamically presents country picker scene with an option of including `Selection Control`.
    ///
    /// By default, invoking this function without defining `selectionControlEnabled` parameter. Its set to `True`
    /// unless otherwise and the `Selection Control` will be included into the list view.
    ///
    /// - Parameter selectionControlEnabled: Section Control State. By default its set to `True` unless otherwise.
    
    func presentCountryPickerScene(withSelectionControlEnabled selectionControlEnabled: Bool = true) {
        switch selectionControlEnabled {
        case true:
            // Present country picker with `Section Control` enabled
            let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
                
                guard let self = self else { return }
                
                self.imvCountry.isHidden = false
                self.imvCountry.image = country.flag
                self.lblCountry.text = country.dialingCode
                self.countryCode = country.dialingCode!
            }
            
            countryController.flagStyle = .circular
            
        case false:
            // Present country picker without `Section Control` enabled
            let countryController = CountryPickerController.presentController(on: self) { [weak self] (country: Country) in
                
                guard let self = self else { return }
                
                self.imvCountry.isHidden = false
                self.imvCountry.image = country.flag
                self.lblCountry.text = country.dialingCode
                self.countryCode = country.dialingCode!
            }
            
            countryController.flagStyle = .corner
            
        }
    }
}
