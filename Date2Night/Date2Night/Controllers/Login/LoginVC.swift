//
//  LoginVC.swift
//  Date2Night
//
//  Created by Developer on 4/17/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON

class LoginVC: BaseViewController {
    
    @IBOutlet weak var btnCheck: UIButton!
    var isChecked = false
    var user = UserModel()
    var fb_id = ""
    var phone = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let id = UserDefaults.standard.string(forKey: Const.KEY_USERID) {
            self.getUser(id)
        }
    }
    
    func getUser(_ id: String) {        
        
        self.showLoadingView()
        ApiRequest.shared.get_user(id) { (code, message, user) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS {
                currentUser = user
                self.createMenuView()
            } else {
                self.showAlert(message!)
            }
        }
    }
    
    @IBAction func onFBLogin(_ sender: Any) {
        
        if isChecked {
            loginFacebook()
        } else {
            self.showAlert("Please review and agree to terms and privacy before login")
        }
    }
    
    func loginFacebook() {
        
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                    fbLoginManager.logOut()
                }
            } else {
                print("Login failed")
            }
        }
    }
    
    func getFBUserData(){
        
        if let token = AccessToken.current, !token.isExpired {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, first_name, last_name"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if error != nil { return }
                    
                let jsonResponse = JSON(result!)
                
                print("jsonResponse", jsonResponse)
                
                let id = jsonResponse["id"].intValue
                self.user.fb_id = id
                let email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", id)
                self.user.name = jsonResponse["name"].string ?? "unknown"
                self.user.photo_url = "https://graph.facebook.com/\(id)/picture?type=large"
                
                print("FB result", self.user.name, email, id, self.user.photo_url)
                
                self.fbLoginProcess()
            })
            
        } else {
            print("token is null")
        }
    }
    
    func fbLoginProcess() {
        
        self.showLoadingView()
        ApiRequest.shared.fb_login(user) { (result, message, value) in
            self.hideLoadingView()
            if result != Const.CODE_FAIL {
                if let token = currentUser?.token, token != "" {
                    ApiRequest.shared.register_token(currentUser!.token) { (msg) in
                    }
                }
                self.createMenuView()
            } else {
                self.showAlert(message!)
            }
        }
    }
    
    @IBAction func onPhoneLogin(_ sender: Any) {
        if isChecked {
            gotoPhoneLogin()
        } else {
            self.showAlert("Please review and agree to terms and privacy before login")
        }
    }
    
    func gotoPhoneLogin() {
        
        pushVC("PhoneNumberVC")
        
    }
    
    @IBAction func onClickTerms(_ sender: Any) {
        
        guard let url = URL(string: "https://date2nightapp.wordpress.com/terms-of-use") else { return }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func onClickPrivacy(_ sender: Any) {
        
        guard let url = URL(string: "https://date2nightapp.wordpress.com/privacy-policy") else { return }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func onCheck(_ sender: Any) {
        
        if isChecked {
            btnCheck.setImage(UIImage(named: "ic_unchecked"), for: .normal)
        } else {
            btnCheck.setImage(UIImage(named: "ic_checked"), for: .normal)
        }
        isChecked = !isChecked
    }
}
