
import UIKit

import MBProgressHUD


class BaseViewController: UIViewController {

    var hud: MBProgressHUD?    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func pushVC(_ vcName: String) {
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: vcName)
        toVC!.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(toVC!, animated: true)
    }
    
    func popVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func popToRootVC(){
        self.navigationController?.popToRootViewController(animated: false)        
    }
    
    func presentVC(_ nameVC: String){
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: nameVC)
        toVC!.modalPresentationStyle = .fullScreen
        self.present(toVC!, animated: false, completion: nil)
    }
    
    func dismissVC() {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    func createMenuView() {
       
        let mainViewController = storyboard!.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        let leftViewController = storyboard!.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC

        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)

        UINavigationBar.appearance().tintColor = UIColor.white
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        nvc.navigationBar.titleTextAttributes = textAttributes

        leftViewController.mainVC = nvc

        let slideMenuController = ExSlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController

        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()

    }
    
    internal func showAlert(title: String?, message: String?, okButtonTitle: String, cancelButtonTitle: String?, okClosure: (() -> Void)?) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let yesAction = UIAlertAction(title: okButtonTitle, style: .default, handler: { (action: UIAlertAction) in
            
            if okClosure != nil {
                okClosure!()
            }
        })
        alertController.addAction(yesAction)
        if cancelButtonTitle != nil {
            let noAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { (action: UIAlertAction) in
                
            })
            alertController.addAction(noAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showAlert(_ message: String) {
        
        showAlertDialog(title: Const.APPNAME, message: message, positive: Const.OK, negative: nil)
    }
    
    func showLoadingView() {

        let window = UIApplication.shared.keyWindow?.rootViewController

        if window != nil {
            hud = MBProgressHUD .showAdded(to: window!.view, animated: true)
        } else {
            hud = MBProgressHUD()
        }
        hud!.mode = .indeterminate
        //hud.label.text = "Loading";
        hud!.animationType = .zoomIn
        hud!.tintColor = UIColor.gray
        hud!.contentColor = Const.COLOR_MAIN
    }

    func hideLoadingView() {
        if let hud = hud {
            hud.hide(animated: true)
        }
    }
    
    @IBAction func onBackVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func signOut() {
        
        let user = UserModel()
        currentUser = user
        ApiRequest.shared.register_token("") { (msg) in }
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        let loginNavVC = self.storyboard?.instantiateViewController(withIdentifier: "loginNav") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = loginNavVC
    }
    
    func showLoadingView(vc: UIViewController) {
        
        hud = MBProgressHUD .showAdded(to: vc.view, animated: true)
        
        hud!.mode = .indeterminate
        //hud.label.text = "Loading";
        hud!.animationType = .zoomIn
        hud!.tintColor = UIColor.gray
        hud!.contentColor = Const.COLOR_MAIN
    }
    
    
}


