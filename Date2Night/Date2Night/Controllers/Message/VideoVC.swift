//
//  VideoVC.swift
//  Date2Night
//
//  Created by Developer on 6/2/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class VideoVC: BaseViewController {
    
    @IBOutlet weak var videoView: VideoView!
    
    var videoUrl: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initVideoView()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        videoView.stop()
    }
    
    func initVideoView() {
        
        if let videoUrl = videoUrl {
            videoView.configure(url: videoUrl)
            videoView.isLoop = true
            videoView.play()
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        popVC()
    }

}
