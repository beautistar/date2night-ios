//
//  MessagingVC.swift
//  Finder
//
//  Created by Developer on 3/23/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
//import FirebaseStorage

class MessagingVC: BaseViewController, KeyboardHandler {
    
    
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var expandButton: UIButton!
    @IBOutlet weak var barBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var actionButtons: [UIButton]!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblBlockedStatus: UILabel!
    @IBOutlet weak var vwBlockLabel: UIView!
    
    //MARK: Private properties
    var messages = [MessageModel]()
    var target_user = UserModel()
    var room_id = ""
    var message = ""
    
    var attachData = [String]()
    
    var block = 0
    var bottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets.bottom + 50
        } else {
            // Fallback on earlier versions
            return 50
        }
    }
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = target_user.name
        fetchMessages()
        
        addKeyboardObservers() {[weak self] state in
            guard state else { return }
            self?.tableView.scroll(to: .bottom, animated: true)
        }
        
        if from == .message {
            block = target_user.block
            self.showBlockLabel()
            
        } else if from == .video {
            sendVideoMessage()
        } else {
            ApiRequest.shared.get_block(target_user.id) { (code, message, block) in
                if code == Const.CODE_SUCCESS {
                    self.block = block as! Int
                    self.showBlockLabel()
                }
            }
            
        }
    }
    
    func showBlockLabel() {
        if block == 1 {
            self.vwBlockLabel.isHidden = false
            self.lblBlockedStatus.text = "You has been blocked by this user"
        } else if block == 2 {
            self.vwBlockLabel.isHidden = false
            self.lblBlockedStatus.text = "You blocked this user"
        } else {
            self.vwBlockLabel.isHidden = true
        }
    }
    
    @IBAction func onClickMore(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) -> Void in
            self.showAlert(title: Const.APPNAME, message: "Are you sure want to delete?", okButtonTitle: Const.YES, cancelButtonTitle: Const.NO) {
                self.removeMessage()
            }
        }))
        var title = "Block"
        if block == 2 {
            title = "Unblock"
        }
        actionSheet.addAction(UIAlertAction(title: title, style: .destructive, handler: { (action) -> Void in
            if self.block == 2 {
                self.showLoadingView()
                ApiRequest.shared.unblock_user(self.target_user.id) { (code, message, value) in
                    self.hideLoadingView()
                    if code == Const.CODE_SUCCESS {
                        self.showAlert("You unblocked this user")
                        self.block = 0
                        self.showBlockLabel()
                    } else {
                        self.showAlert(message!)
                    }
                }
            } else if self.block == 0 {
                self.showLoadingView()
                ApiRequest.shared.block_user(self.target_user.id) { (code, message, value) in
                    self.hideLoadingView()
                    if code == Const.CODE_SUCCESS {
                        self.showAlert("You blocked this user")
                        self.block = 2
                        self.showBlockLabel()
                    } else {
                        self.showAlert(message!)
                    }
                }
            } else {
                self.showAlert("You has already been blocked by this user")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func sendMessagePressed(_ sender: Any) {
            
        guard let text = inputTextField.text, !text.isEmpty else { return }

        let message = MessageModel()
        message.content_type = "text"
        message.message = text
        message.sender_id = "\(currentUser!.id)"
        send(message)
            
        if messages.count == 0 {
            ApiRequest.shared.add_inbox(target_user.id) { (result, message, value) in
            }
        }
    }
    
    func sendVideoMessage() {
        
        let message = MessageModel()
        message.content_type = "photo"
        message.message = attachData[0]
        message.sender_id = "\(currentUser!.id)"
        message.video_url = attachData[1]
        send(message)
            
        if messages.count == 0 {
            ApiRequest.shared.add_inbox(target_user.id) { (result, message, value) in
            }
        }
    }
}

//MARK: Private methods
extension MessagingVC {
    
    private func fetchMessages() {
        
        if currentUser!.id < target_user.id {
            room_id = "\(currentUser!.id)_\(target_user.id)"
        } else {
            room_id = "\(target_user.id)_\(currentUser!.id)"
        }
        
        messages.removeAll()
        
        let database = Database.database().reference()
        database.child("message")
        .child(room_id)
        .observe(.childAdded) { (snapshot) in
            print("snapshot===", snapshot.value ?? "" as Any)
            if let messageData = snapshot.value as? [String:AnyObject] {
                var objFirst = MessageModel()
                objFirst =  MessageModel.parseMessageData(ary: NSArray(object:messageData)).object(at: 0) as! MessageModel
                objFirst.id = snapshot.key
                self.messages.append(objFirst)
            }
            self.messages = self.messages.sorted(by: {$0.time < $1.time})
            self.tableView.reloadData()
            self.tableView.scroll(to: .bottom, animated: true)
        }
    }
  
    private func send(_ message: MessageModel) {
            
        if message.content_type == "text" && ((inputTextField.text == "" || inputTextField.text == " ")) {
            self.showAlertDialog(title: Const.APPNAME, message: "Type message", positive: Const.OK, negative: nil)
        } else {
        
            if target_user.id != 0 {
                addNotiCount()
            }
            
            var messageData = [String: Any]()
            
            messageData = ["message" : message.message,
                           "content_type": message.content_type,
                           "video_url": message.video_url,
                           "receiver_id" : target_user.id,
                           "receiver_img": target_user.photo_url,
                           "receiver_name": "\(target_user.name)",
                           "sender_id" : "\(currentUser!.id)",
                           "sender_img" : "\(currentUser!.photo_url)",
                           "sender_name" : "\(currentUser!.name)",
                           "time" : Date().toMillis()!]

            let key = Database.database().reference().child("message").child(room_id).childByAutoId().key!
            Database.database().reference().child("message").child(room_id).child(key).setValue(messageData)
            self.message = inputTextField.text!
            self.inputTextField.text = ""
        }
    }
    
    private func removeMessage() {
        let ref = Database.database().reference()
        let reference = ref.child("message").child(room_id)
        reference.removeValue { error, _ in
            guard let error = error else {
                self.messages.removeAll()
                self.tableView.reloadData()
                ApiRequest.shared.delete_inbox(self.target_user.id) { (code, message, value) in                    
                }
                return
            }
            print(error.localizedDescription)
        }
    }
    
    private func removeVideoMessage(index: Int) {
        
        let ref = Database.database().reference()
        let reference = ref.child("message").child(room_id).child(messages[index].id)
        reference.removeValue { error, _ in
            guard let error = error else {
                self.messages.remove(at: index)
                self.tableView.reloadData()
                return
            }
            print(error.localizedDescription)
        }        
    }
    
    func addNotiCount() {
        ApiRequest.shared.add_noti_count(target_user.id, message: self.message) { (result, message, value) in
        }
    }
  
    private func showActionButtons(_ status: Bool) {
        guard !status else {
            stackViewWidthConstraint.constant = 112
            UIView.animate(withDuration: 0.3) {
                self.expandButton.isHidden = true
                self.expandButton.alpha = 0
                self.actionButtons.forEach({$0.isHidden = false})
                self.view.layoutIfNeeded()
            }
            return
        }
        guard stackViewWidthConstraint.constant != 32 else { return }
        stackViewWidthConstraint.constant = 32
        UIView.animate(withDuration: 0.3) {
            self.expandButton.isHidden = false
            self.expandButton.alpha = 1
            self.actionButtons.forEach({$0.isHidden = true})
            self.view.layoutIfNeeded()
        }
    }
}

//MARK: IBActions
extension MessagingVC {
  
    @objc func onClickprofile(_ sender: UIButton) {
        
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        let user = UserModel()
        user.id = target_user.id
        profileVC.user = user
        from = .other
        profileVC.modalPresentationStyle = .fullScreen
        self.present(profileVC, animated: false, completion: nil)
    }
    
  
    @IBAction func sendImagePressed(_ sender: UIButton) {
        //imageService.pickImage(from: self, allowEditing: false, source: sender.tag == 0 ? .photoLibrary : .camera) {[weak self] image //in
        //let message = MessageModel()
        //message.contentType = .photo
        // message.sender_id = "\(currentUser!.user_id)"
        //self?.send(message)
        //self?.inputTextField.text = nil
        //self?.showActionButtons(false)
        //}
  }
  
    @IBAction func sendLocationPressed(_ sender: UIButton) {
        /*
         locationService.getLocation {[weak self] response in
         switch response {
         case .denied:

            self?.showAlertDialog(title: Const.APP_NAME, message: Const.MSG_ENABLE_LOCATION, positive: Const.OK, negative: nil)
            case .location(let location):
            let message = MessageModel()
            message.sender_id = "\(currentUser!.user_id)"
            message.message = location.string
            message.contentType = .location
            //self?.send(message)
            self?.inputTextField.text = nil
            //self?.showActionButtons(false)
          }
        }*/
  }
  
    @IBAction func expandItemsPressed(_ sender: UIButton) {
        //showActionButtons(false)
        //showActionButtons(true)
    }
    
}

//MARK: UITableView Delegate & DataSource
extension MessagingVC: UITableViewDelegate, UITableViewDataSource {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        
        let senderid = currentUser!.id
        if message.content_type == "text" {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: message.sender_id == "\(senderid)" ? "MessageTableViewCell" : "UserMessageTableViewCell") as! MessageTableViewCell
            cell.set(message)
            cell.profilePic!.borderWidth = 1
            if target_user.membership_type != -1 {
                cell.profilePic!.borderColor = UIColor.yellow
            } else {
                cell.profilePic!.borderColor = .white
            }
            cell.btnProfile.tag = indexPath.row
            cell.btnProfile.addTarget(self, action: #selector(onClickprofile(_:)), for: .touchUpInside)
            return cell
        } else {
        
            let identifier = message.sender_id == "\(senderid)" ? "MessageAttachmentTableViewCell" : "UserMessageAttachmentTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! MessageAttachmentTableViewCell
            cell.set(message)
            cell.profilePic?.borderWidth = 1.0
            cell.profilePic!.borderWidth = 1
            if target_user.membership_type != -1 {
                cell.profilePic!.borderColor = UIColor.yellow
            } else {
                cell.profilePic!.borderColor = .white
            }
            cell.btnProfile.tag = indexPath.row
            cell.btnProfile.addTarget(self, action: #selector(onClickprofile(_:)), for: .touchUpInside)
            return cell
        }
    }
  
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard tableView.isDragging else { return }
        cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            cell.transform = CGAffineTransform.identity
        })
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = messages[indexPath.row]
        switch message.content_type {
        case "photo":
            let playVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoVC") as! VideoVC
            playVC.videoUrl = message.video_url
            self.navigationController?.pushViewController(playVC, animated: true)
            if message.sender_id != "\(currentUser!.id)" {
                removeVideoMessage(index: indexPath.row)
            }
        default: break
        }
    }
}

//MARK: UItextField Delegate
extension MessagingVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
  
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //showActionButtons(false)
        return true
    }
}

//MARK: MessageTableViewCellDelegate Delegate
extension MessagingVC: MessageTableViewCellDelegate {

    func messageTableViewCellUpdate() {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
//MARK: Date Extention
extension Date {
    
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
    init(millis: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(millis / 1000))
        self.addTimeInterval(TimeInterval(Double(millis % 1000) / 1000 ))
    }
}

