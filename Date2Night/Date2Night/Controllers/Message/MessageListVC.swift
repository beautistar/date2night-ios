//
//  MessageListVC.swift
//  Date2Night
//
//  Created by Developer on 4/17/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit


class MessageListVC: BaseViewController {
    
    @IBOutlet weak var cvUserList: UICollectionView!
    var userList = [UserModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController!.navigationBar.titleTextAttributes = textAttributes
        
        if from == .message {
            setNavigationBarItem()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInboxUser()
    }
    
    func getInboxUser() {
        
        self.showLoadingView(vc: self)
        ApiRequest.shared.get_inbox_user { (result, message, users) in
            self.hideLoadingView()
            if result == Const.CODE_SUCCESS {
                self.userList = users!
                self.cvUserList.reloadData()
            } else {
                self.showAlert(message!)
            }
        }
    }
}

extension MessageListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SmallCircleCell", for: indexPath) as! SmallCircleCell
        let user = userList[indexPath.row]
        cell.lblTitle.text = user.name
        cell.imvPhoto.kf.indicatorType = .activity
        cell.imvPhoto.kf.setImage(with: URL(string: user.photo_url), placeholder: Const.PLACEHOLDER_PROFILE)
        if user.noti_count == 0 {
            cell.lblBadge.isHidden = true
        } else {
            cell.lblBadge.isHidden = false
            cell.lblBadge.text = "\(user.noti_count)"
        }
        cell.imvPhoto.borderWidth = 2.0
        if user.membership_type != -1 {
            cell.imvPhoto.borderColor = UIColor.yellow            
        } else {
            cell.imvPhoto.borderColor = .white
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
        return CGSize(width: (self.view.size.width - 50) / 3.0, height: (self.view.size.width - 50) / 3.0 + 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let targetUser = userList[indexPath.row]
        
        ApiRequest.shared.delete_noti_count(targetUser.id, count: targetUser.noti_count) { (code, message, value) in
            self.userList[indexPath.row].noti_count = 0
            self.cvUserList.reloadData()
            
            let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "MessagingVC") as! MessagingVC
            chatVC.target_user = targetUser
            from = .message
            self.navigationController?.pushViewController(chatVC, animated: true)
        }
    }
}
