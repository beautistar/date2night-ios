//
//  MenuViewController.swift
//  Pixil
//
//  Created by Developer on 7/25/19.
//  Copyright © 2019 Ankhbayar Batbaatar. All rights reserved.
//

import UIKit
import Kingfisher

enum LeftMenu: Int {
    case main = 0
    case message
    case preference
    case notification
    case mypost
    case purchase
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}


class MenuVC: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, LeftMenuProtocol {
   
    @IBOutlet weak var tblmenu: UITableView!
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var menus = ["Home", "Messages", "Preferences", "Notifications", "My Posts", "Purchase"]
    var icons = ["ic_pin", "chat", "ic_setting", "ic_notification", "ic_mypost", "ic_cart"]
    
    
    var mainVC: UIViewController!    
    var messageListVC: UIViewController!
    var preferenceVC: UIViewController!
    var purchaseVC: UIViewController!
    var mypostVC: UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        
        imvPhoto.layer.borderWidth = 2.0
        imvPhoto.layer.borderColor =  UIColor.white.cgColor
        
        if UserDefaults.standard.string(forKey: "notification") == nil {
            UserDefaults.standard.set("yes", forKey: "notification")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {       
        
        super.viewWillAppear(animated)
        
        if let user = currentUser {
            lblName.text = user.name
            imvPhoto.kf.setImage(
                with: URL(string: user.photo_url),
            placeholder: UIImage(named: "profile"))
            if user.membership_type != -1 {
                imvPhoto.borderColor = UIColor.yellow
            } else {
                imvPhoto.borderColor = .white
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func initView() {
        
        tblmenu.tableFooterView = UIView()

        let mainVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        self.mainVC = UINavigationController(rootViewController: mainVC)

        let messageListVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageListVC") as! MessageListVC
        self.messageListVC = UINavigationController(rootViewController: messageListVC)

        let preferenceVC = self.storyboard?.instantiateViewController(withIdentifier: "PreferenceVC") as! PreferenceVC
        self.preferenceVC = UINavigationController(rootViewController: preferenceVC)

        let purchaseVC = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseVC") as! PurchaseVC
        self.purchaseVC = UINavigationController(rootViewController: purchaseVC)
        
        let myPostVC = self.storyboard?.instantiateViewController(withIdentifier: "MyPostVC") as! MyPostVC
        self.mypostVC = UINavigationController(rootViewController: myPostVC)

    }

    // MARK: - left menu protocol
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
        case .main:
            from = .main            
            self.slideMenuController()?.changeMainViewController(self.mainVC, close: true)
        case .message:
            from = .message
            self.slideMenuController()?.changeMainViewController(self.messageListVC, close: true)
        case .preference:
            from = .preference
            self.slideMenuController()?.changeMainViewController(self.preferenceVC, close: true)
        
        case .purchase:
            from = .purchase
            self.slideMenuController()?.changeMainViewController(self.purchaseVC, close: true)
        case .notification:
            break            
        case .mypost:
            from = .mypost
            self.slideMenuController()?.changeMainViewController(self.mypostVC, close: true)            
        }
    }
    
    @objc func onSwitchNotification(_ sender: UISwitch) {
        
        if sender.isOn {
            UIApplication.shared.registerForRemoteNotifications()
            UserDefaults.standard.set("yes", forKey: "notification")
        } else {
            
            UserDefaults.standard.set("no", forKey: "notification")
            UIApplication.shared.unregisterForRemoteNotifications()
        }
    }
    
    // MARK: - Actions
    @IBAction func onLogOut(_ sender: Any) {
        
        signOut()
        
    }
    
    @IBAction func onClickProfile(_ sender: Any) {
        from = .profile
        self.slideMenuController()?.closeLeft()
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        profileVC.user = currentUser
        profileVC.modalPresentationStyle = .fullScreen
        self.present(profileVC, animated: false, completion: nil)
    }
    
    @IBAction func onClickPost(_ sender: Any) {
        
        self.slideMenuController()?.closeLeft()
        let postVC = self.storyboard?.instantiateViewController(withIdentifier: "PostVC") as! PostVC
        postVC.modalPresentationStyle = .fullScreen
        self.present(postVC, animated: false, completion: nil)
    
    }
    
    @IBAction func onClickVideoPost(_ sender: Any) {      
        
        self.slideMenuController()?.closeLeft()
        let uploadVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadVideoVC") as! UploadVideoVC
        uploadVC.modalPresentationStyle = .fullScreen
        self.present(uploadVC, animated: false, completion: nil)
    
    }
    
    // MARK: -  UITableView Delegate, Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        
        cell.lblName.text = menus[indexPath.row]
        cell.imvIcon.image = UIImage(named: icons[indexPath.row])
        if indexPath.row == 3 {
            cell.switchNotification.isHidden = false
            if UserDefaults.standard.string(forKey: "notification") == "yes" {
                cell.switchNotification.setOn(true, animated: false)
            } else {
                cell.switchNotification.setOn(false, animated: false)
            }
            cell.switchNotification.addTarget(self, action: #selector(onSwitchNotification(_:)), for: .valueChanged)
        } else {
            cell.switchNotification.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblmenu == scrollView {
            
        }
    }
}
