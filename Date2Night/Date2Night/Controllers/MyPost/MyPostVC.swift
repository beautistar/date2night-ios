//
//  MyPostVC.swift
//  Date2Night
//
//  Created by Developer on 4/17/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class MyPostVC: BaseViewController {
    
    @IBOutlet weak var topCollectionView: UICollectionView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    var posts = [PostModel]()
    var small_posts = [SmallPostModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Posts"
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController!.navigationBar.titleTextAttributes = textAttributes
        setNavigationBarItem()
       
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refresh))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getMyPost()
        getMySmallPost()
    }
    
    func getMyPost() {
        
        self.showLoadingView()
        
        ApiRequest.shared.get_my_post { (code, message, posts) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS {
                self.posts = posts!
                self.mainCollectionView.reloadData()
            }
        }
    }
    
    @objc func refresh() {
        
        getMyPost()
        getMySmallPost()
    }
    
    func getMySmallPost() {
        
        ApiRequest.shared.get_my_small_post { (code, message, posts) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS {
                self.small_posts = posts!
                self.topCollectionView.reloadData()
            }
        }
    }
    
    @objc func deletePost(_ sender: UIButton) {
        
        self.showAlert(title: Const.APPNAME, message: "Are you sure want to delete your posts?", okButtonTitle: "Yes", cancelButtonTitle: "No") {
            self.showLoadingView()
            ApiRequest.shared.delete_post(self.posts[sender.tag].id) { (code, message, value) in
                self.hideLoadingView()
                if code == Const.CODE_SUCCESS {
                    self.posts.remove(at: sender.tag)
                    self.mainCollectionView.reloadData()
                } else {
                    self.showAlert(message!)
                }
            }
        }
    }
    
    @objc func deleteSmallPost(_ sender: UIButton) {
        
        self.showAlert(title: Const.APPNAME, message: "Are you sure want to delete your posts?", okButtonTitle: "Yes", cancelButtonTitle: "No") {
            self.showLoadingView()
            ApiRequest.shared.delete_small_post(self.small_posts[sender.tag].id) { (code, message, value) in
                self.hideLoadingView()
                if code == Const.CODE_SUCCESS {
                    self.small_posts.remove(at: sender.tag)
                    self.topCollectionView.reloadData()
                } else {
                    self.showAlert(message!)
                }
            }
        }
    }
}


extension MyPostVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == topCollectionView {
            return small_posts.count
        } else {
            return posts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
        if collectionView == topCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SmallCircleCell", for: indexPath) as! SmallCircleCell
            cell.entity = small_posts[indexPath.row]
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(deleteSmallPost(_:)), for: .touchUpInside)
            return cell
        
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LargeCircleCell", for: indexPath) as! LargeCircleCell
            cell.entity = posts[indexPath.row]
            cell.imvPhoto.frame.size.height = (self.view.size.width - 50) / 2.0
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(deletePost(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == topCollectionView {
            return CGSize(width: 90, height: 80)
        } else {
            return CGSize(width: (self.view.size.width - 50) / 2.0, height: (self.view.size.width - 50) / 2.0)
        }
    }
}
