//
//  MainVC.swift
//  Date2Night
//
//  Created by Developer on 4/17/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import AVKit


class MainVC: BaseViewController, UIGestureRecognizerDelegate, UISearchBarDelegate {

    @IBOutlet weak var topCollectionView: UICollectionView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var posts = [PostModel]()
    var totalPosts = [PostModel]()
    var smallPosts = [SmallPostModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        from = .main
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.endEditing(true)
        
        loadData()
    }
    
    override func viewDidLayoutSubviews() {
        setupSearchBar(searchBar: searchBar)
    }    

    func setupSearchBar(searchBar : UISearchBar) {
        searchBar.setPlaceholderTextColorTo(color: UIColor.white)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func initView() {
        
        setNavigationBarItem()
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "chat"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.gotoChat), for: .touchUpInside)
        let rightButton = UIBarButtonItem(customView: btn1)
        navigationItem.rightBarButtonItem = rightButton
        
        let container = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 40))

        let logo = UIImage(named: "Date2Night_title")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(imageView)

        let leftButtonWidth: CGFloat = 80 // left padding
        let rightButtonWidth: CGFloat = 80 // right padding
        let width = view.frame.width - leftButtonWidth - rightButtonWidth
        let offset = (rightButtonWidth - leftButtonWidth) / 2

        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: container.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            imageView.centerXAnchor.constraint(equalTo: container.centerXAnchor, constant: -offset),
            imageView.widthAnchor.constraint(equalToConstant: width)
        ])
        self.navigationItem.titleView = imageView
        
        searchBar.delegate = self
        
        let refresher = UIRefreshControl()
        mainCollectionView!.alwaysBounceVertical = true
        refresher.tintColor = .clear
        refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        mainCollectionView!.refreshControl = refresher
    }
    
    @objc func loadData() {
        
        getNotiCount()
        
        if isFiltered {
            self.getFilterPost()
        } else {
            getPost()
        }
        getSmallPost()
        
    }
    
    func getNotiCount() {
        
        ApiRequest.shared.get_noti_count { (code, message, count) in
            if code == Const.CODE_SUCCESS {
                currentUser?.noti_count = count
                if let user = currentUser {
                    if user.noti_count == 0 {
                        self.navigationItem.rightBarButtonItem?.setBadge(text: "")
                    } else  {
                        self.navigationItem.rightBarButtonItem?.setBadge(text: "\(user.noti_count)")
                    }
                }
            }
        }
    }
    
    func getPost() {
        
        self.showLoadingView()
        
        ApiRequest.shared.get_post { (code, message, posts) in
            self.hideLoadingView()
            self.mainCollectionView.refreshControl?.endRefreshing()
            if code == Const.CODE_SUCCESS {
                self.posts = posts!
                self.totalPosts = posts!
                
                self.mainCollectionView.reloadData()
            }
        }
    }
    
    func getFilterPost() {
        
        self.showLoadingView()
        
        ApiRequest.shared.filter_post { (code, message, posts) in
            self.hideLoadingView()
            self.mainCollectionView.refreshControl?.endRefreshing()
            if code == Const.CODE_SUCCESS {
                self.posts = posts!
                
                self.mainCollectionView.reloadData()
            }
        }
    }
    
    func getSmallPost() {
        
        ApiRequest.shared.get_small_post { (code, message, posts) in
            
            if code == Const.CODE_SUCCESS {
                self.smallPosts = posts!
                self.topCollectionView.reloadData()
            }
        }
    }
    
    func viewPost(post_id: Int) {
        
        ApiRequest.shared.view_post(post_id) { (result, message, value) in
        }
    }
    
    @objc func gotoChat() {
        let msgListVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageListVC") as! MessageListVC
        self.navigationController?.pushViewController(msgListVC, animated: true)
    }
    
    @objc func handleLongPress(gesture: UILongPressGestureRecognizer) {
        
        //if gesture.state != .ended { return }
        
        let p = gesture.location(in: self.topCollectionView)
        if let indexPath = self.topCollectionView.indexPathForItem(at: p) {
            
            self.showAlert(title: nil, message: "Are you sure want to report this post?", okButtonTitle: Const.YES, cancelButtonTitle: Const.NO) {
                
                self.reportSmallPost(post_id: self.smallPosts[indexPath.row].id)
            }
        }
    }
    
    func reportSmallPost(post_id: Int) {
        self.showLoadingView()
        ApiRequest.shared.report_small_post(post_id) { (code, message, value) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS {
                self.showAlert("This post is reported")
            } else {
                self.showAlert(message!)
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.count > 1 {
                
            posts = totalPosts.filter{ ($0.activity.lowercased().contains(searchBar.text!.lowercased())) }
            mainCollectionView.reloadData()
        } else {
            posts = totalPosts
            mainCollectionView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}

extension MainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == topCollectionView {
            return smallPosts.count
        } else {
            return posts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == topCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SmallCircleCell", for: indexPath) as! SmallCircleCell
            cell.entity = smallPosts[indexPath.row]
            
            let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
            lpgr.minimumPressDuration = 0.5
            lpgr.delegate = self
            lpgr.delaysTouchesBegan = true
            cell.addGestureRecognizer(lpgr)
            
            return cell
        
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LargeCircleCell", for: indexPath) as! LargeCircleCell
            cell.entity = posts[indexPath.row]        
            cell.imvPhoto.frame.size.height = (self.view.size.width - 50) / 2.0
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == topCollectionView {            
            return CGSize(width: 90, height: 80)
        } else {
            return CGSize(width: (self.view.size.width - 50) / 2.0, height: (self.view.size.width - 50) / 2.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == topCollectionView {

            let playVC = self.storyboard?.instantiateViewController(withIdentifier: "VIdeoPlayVC") as! VIdeoPlayVC
            from = .main
            playVC.smallPost = self.smallPosts[indexPath.row]
            self.navigationController?.pushViewController(playVC, animated: true)
        } else {
            let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
            detailVC.post = posts[indexPath.row]
            self.navigationController?.pushViewController(detailVC, animated: true)
            
            self.viewPost(post_id: posts[indexPath.row].id)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == topCollectionView {
            return 0
        }
        
        return 10
    }
    
    
}

extension UISearchBar
{
    func setPlaceholderTextColorTo(color: UIColor)
    {
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = color
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = color
    }
}


extension MainVC : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}

