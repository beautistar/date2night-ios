//
//  VIdeoPlayVC.swift
//  Date2Night
//
//  Created by Developer on 5/1/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class VIdeoPlayVC: BaseViewController {

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var videoView: VideoView!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txvDescription: UITextView!
    var smallPost = SmallPostModel()
    var videoUrl: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if from == .main {
            self.infoView.isHidden = false
            self.txvDescription.isHidden = false
            initView()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        videoView.stop()
    }
    
    func initView() {
        
        self.title = smallPost.title
        txvDescription.text = smallPost.description
        imvPhoto.borderWidth = 1
        if smallPost.highlighted == "yes" {
            imvPhoto.borderColor = .yellow
        } else {
            imvPhoto.borderColor = UIColor(named: "BorderColor")
        }
        
        imvPhoto.kf.setImage(with: URL(string: smallPost.user_photo), placeholder: Const.PLACEHOLDER_PROFILE)
        
        if smallPost.video_url != "" {
            videoView.configure(url: smallPost.video_url)
            videoView.isLoop = true
            videoView.play()
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickProfile(_ sender: Any) {
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        let user = UserModel()
        user.id = smallPost.user_id
        profileVC.user = user
        from = .other
        profileVC.modalPresentationStyle = .fullScreen
        self.present(profileVC, animated: false, completion: nil)
        videoView.stop()
    }
    
    @IBAction func onClickChat(_ sender: Any) {
        let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "MessagingVC") as! MessagingVC
        let user = UserModel()
        user.id = smallPost.user_id
        user.photo_url = smallPost.user_photo
        messageVC.target_user = user
        from = .main
        self.navigationController?.pushViewController(messageVC, animated: true)
        videoView.stop()
    }
}
