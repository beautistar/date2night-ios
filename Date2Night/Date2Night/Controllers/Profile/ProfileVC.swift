//
//  ProfileVC.swift
//  Date2Night
//
//  Created by Developer on 4/20/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import FSPagerView
import iOSDropDown
import MBProgressHUD

class ProfileVC: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var numberOfItems = 3
    @IBOutlet weak var vwAddPhoto: UIView!
    @IBOutlet weak var btn3dot: UIButton!
    
    @IBOutlet weak var vwViewPhoto: FSPagerView! {
        didSet {
            self.vwViewPhoto.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        
            self.vwViewPhoto.transformer = FSPagerViewTransformer(type: .linear)
            let newScale: CGFloat = 0.9
            self.vwViewPhoto.itemSize = self.vwViewPhoto.frame.size.applying(CGAffineTransform(scaleX: newScale, y: newScale))
            self.vwViewPhoto.interitemSpacing = 10
        }
    }
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.numberOfPages = numberOfItems
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 30, right: 20)
            self.pageControl.itemSpacing = 8.0
            self.pageControl.hidesForSinglePage = true
            self.pageControl.setStrokeColor(.white, for: .normal)
            self.pageControl.setStrokeColor(.white, for: .selected)
            self.pageControl.setFillColor(.white, for: .selected)
        }
    }
    
    @IBOutlet weak var tfGender: DropDown!
    @IBOutlet weak var tfOrientation: DropDown!
    
    var attachments = [(Data, String)]()
    
    let picker: UIImagePickerController = UIImagePickerController()
    var user: UserModel!
    var selectedPhotoIndex = 0
    @IBOutlet weak var imvPhoto1: UIImageView!
    @IBOutlet weak var imvPhoto2: UIImageView!
    @IBOutlet weak var imvPhoto3: UIImageView!
    
    @IBOutlet weak var tfFullName: UITextField!
    @IBOutlet weak var tfAge: UITextField!
    @IBOutlet weak var tfInstagram: UITextField!
    @IBOutlet weak var tfSnapChat: UITextField!
    @IBOutlet weak var btnInstagram: UIButton!
    @IBOutlet weak var btnSnapChat: UIButton!
    @IBOutlet weak var btnUpload: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.picker.delegate = self
        picker.allowsEditing = true
        
        initView()
    }
    
    func initView() {
        
        attachments.removeAll()
        
        if from == .profile {
            self.user = currentUser
            btn3dot.isHidden = true
            vwAddPhoto.isHidden = false
            vwViewPhoto.isHidden = true
            pageControl.isHidden = true
            tfGender.optionArray = ["Male", "Female"]
            tfGender.didSelect { (text, index, id) in
                self.tfGender.text = text
            }
            
            tfOrientation.optionArray = ["Straight", "Gay", "Bisexual"]
            tfOrientation.didSelect { (text, index, id) in
                self.tfOrientation.text = text
            }
            
            self.navigationItem.rightBarButtonItem?.customView?.isHidden = true
            btnSnapChat.isHidden = true
            btnInstagram.isHidden = true
            self.btnUpload.isHidden = false
            
            setupView()
            
        } else {
            
            self.btn3dot.isHidden = false
            self.btnUpload.isHidden = true
            self.navigationItem.rightBarButtonItem?.customView?.isHidden = false
            btnSnapChat.isHidden = false
            btnInstagram.isHidden = false
            tfFullName.isUserInteractionEnabled = false
            tfGender.arrowColor = .clear
            tfGender.isUserInteractionEnabled = false
            tfAge.isUserInteractionEnabled = false
            tfOrientation.arrowColor = .clear
            tfOrientation.isUserInteractionEnabled = false            
            
            self.showLoadingView(vc: self)
            ApiRequest.shared.get_user("\(user.id)") { (code, message, user) in
                self.hideLoadingView()
                if code == Const.CODE_SUCCESS {
                    self.user = user
                    self.setupView()
                } else {
                    self.showAlert(message!)
                }
            }
        }
    }
    
    func setupView() {
        
        tfFullName.text = user.name
        tfGender.text = user.gender
        tfAge.text = "\(user.age)"
        tfOrientation.text = user.orientation
        tfInstagram.text = user.instagram
        tfSnapChat.text = user.snapchat
        
        if from == .profile { // edit profile            
            imvPhoto1.kf.setImage(with: URL(string: user.photo_url), placeholder: Const.PLACEHOLDER_IMAGE)
            imvPhoto2.kf.setImage(with: URL(string: user.photo_url1))
            imvPhoto3.kf.setImage(with: URL(string: user.photo_url2))
        } else {
            self.vwViewPhoto.reloadData()
        }
    }
    
    func isValid() -> Bool {
        
        if tfFullName.text!.isEmpty {
            self.showAlert("Please input full name")
            return false
        }
        
        if tfGender.text!.isEmpty {
            self.showAlert("Please select gender")
            return false
        }
        
        if tfAge.text!.isEmpty {
            self.showAlert("please select age")
            return false
        }
        
        if Int(tfAge.text!)! < 18 || Int(tfAge.text!)! > 100 {
            self.showAlert("Age must be greater than 17")
            return false
        }
        
        if tfOrientation.text!.isEmpty {
            self.showAlert("Please select orientation")
            return false
        }
        
        return true
    }
    
    @IBAction func onClickInstagram(_ sender: Any) {
        
        if user.instagram == "" { return }
        let instagramHooks = "instagram://user?username=\(user.instagram)"
        let instagramUrl = URL(string: instagramHooks)
        if UIApplication.shared.canOpenURL(instagramUrl!) {
            UIApplication.shared.open(instagramUrl! as URL)
        } else {
          //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.open(URL(string: "http://instagram.com/\(user.instagram)")!)
        }
    }
    
    @IBAction func onClickSnapChat(_ sender: Any) {
        
        if user.snapchat == "" { return }
        let appURL = URL(string: "snapchat://add/\(user.snapchat)")!
        let application = UIApplication.shared

        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            // if Snapchat app is not installed, open URL inside Safari
            let webURL = URL(string: "https://www.snapchat.com/add/\(user.snapchat)")!
            application.open(webURL)
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        
        self.dismissVC()
    }
    
    @IBAction func onClickChange(_ sender: Any) {
        
        if !isValid() { return }
        if let user = currentUser {
            user.name = tfFullName.text!
            user.age = Int(tfAge.text!)!
            user.gender = tfGender.text!
            user.orientation = tfOrientation.text!
            user.instagram = tfInstagram.text!
            user.snapchat = tfSnapChat.text!
            self.user = user
        }
        
        self.showLoadingView(vc: self)
        
        ApiRequest.shared.edit_user(user) { (code, message, value) in
            
            if code == Const.CODE_SUCCESS {
                if self.attachments.count > 0 {
                    self.uploadPhoto()
                } else {
                    self.hideLoadingView()
                    self.dismissVC()
                }
            } else if code == Const.CODE_FAIL {
                self.hideLoadingView()
                self.dismissVC()
                self.showAlert(message!)
            } else {
                self.hideLoadingView()
                self.showAlert(message!)
            }
        }
    }
    
    func uploadPhoto() {
        
        ApiRequest.shared.upload_photo(attachments: attachments) { (result, message, value) in
            
            self.hideLoadingView()
            if result == Const.CODE_SUCCESS {
                self.dismissVC()
            } else {
                self.showAlert(message!)
            }
        }
    }
    
    @IBAction func onClickAddPhoto(_ sender: UIButton) {
        
        selectedPhotoIndex = sender.tag
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) && UIImagePickerController.isSourceTypeAvailable(.camera) {
        
            let actionSheet = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
                self.picker.sourceType = .photoLibrary
                self.present(self.picker, animated: true, completion: nil)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
                self.picker.sourceType = .camera
                self.present(self.picker, animated: true, completion: nil)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClick3Dot(_ sender: Any) {
        
        self.showAlert(title: Const.APPNAME, message: "Are you sure want to block this user?", okButtonTitle: Const.YES, cancelButtonTitle: Const.NO) {
            
        }
    }
    
    //MARK:-ImagePickerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.editedImage] as? UIImage {
            
            if selectedPhotoIndex == 0 {
                self.imvPhoto1.image = image
                if let index = attachments.firstIndex(where: { $0.1 == "photo"}) {
                    attachments.remove(at: index)
                }
                attachments.append((image.jpegData(compressionQuality: 0.75)!, "photo"))
            } else if selectedPhotoIndex == 1 {
                self.imvPhoto2.image = image
                if let index = attachments.firstIndex(where: { $0.1 == "photo1"}) {
                    attachments.remove(at: index)
                }
                attachments.append((image.jpegData(compressionQuality: 0.75)!, "photo1"))
            } else {
                self.imvPhoto3.image = image
                if let index = attachments.firstIndex(where: { $0.1 == "photo2"}) {
                    attachments.remove(at: index)
                }
                attachments.append((image.jpegData(compressionQuality: 0.75)!, "photo2"))
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
}

// MARK:- FSPagerView DataSource
extension ProfileVC: FSPagerViewDataSource,FSPagerViewDelegate {
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return 3
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        switch index {
        case 0:
            cell.imageView?.kf.setImage(with: URL(string: user.photo_url), placeholder: Const.PLACEHOLDER_IMAGE)
        case 1:
            cell.imageView?.kf.setImage(with: URL(string: user.photo_url1), placeholder: Const.PLACEHOLDER_IMAGE)
        default:
            cell.imageView?.kf.setImage(with: URL(string: user.photo_url2), placeholder: Const.PLACEHOLDER_IMAGE)
        }
        cell.imageView?.cornerRadius = 10
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
}
