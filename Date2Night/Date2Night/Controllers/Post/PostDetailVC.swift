//
//  PostDetailVC.swift
//  Date2Night
//
//  Created by Developer on 4/23/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import AVKit

class PostDetailVC: BaseViewController {
    
    @IBOutlet weak var imvPostPhoto: UIImageView!
    @IBOutlet weak var imvUserPhoto: UIImageView!
    @IBOutlet weak var lblViewCount: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblActivity: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var tvSummary: UITextView!
    @IBOutlet weak var imvTimer: UIImageView!
    @IBOutlet weak var vwVideo: VideoView!
    
    var post = PostModel()
    var timer: Timer?
    var totalTime = 24 * 60 * 60
    var isDanged = false
    
    private let imageService = ImagePickerService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let logo = UIImage(named: "Date2Night_title")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        initView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        vwVideo.stop()
    }
    
    func initView() {
        
        imvPostPhoto.kf.setImage(with: URL(string: post.photo_url), placeholder: Const.PLACEHOLDER_IMAGE)
        if post.video_url != "" {
            vwVideo.configure(url: post.video_url)
            vwVideo.isLoop = true
            vwVideo.play()
        }
        imvUserPhoto.kf.setImage(with: URL(string: post.user_photo), placeholder: Const.PLACEHOLDER_PROFILE)
        lblViewCount.text = "\(post.view_count)"
        lblActivity.text = "Activity: \(post.activity)"
        lblLocation.text = "Location: \(post.location)"
        tvSummary.text = post.summary
        //secound is always minus
        totalTime = 24 * 60 * 60 - abs(getDiffSeconds(post.created_at))
        /// set timer
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        
        self.lblTimer.text = self.timeFormatted(self.totalTime) // will show timer
        if totalTime > 0 {
            totalTime -= 1  // decrease counter timer
            if totalTime < 5 * 60 * 60, !isDanged {
                //self.imvTimer.image = UIImage(named: "time_red")
                isDanged = true
            }
        } else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
                //self.imvTimer.image = UIImage(named: "time_red")
                self.lblTimer.text = "-"
            }
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 60 / 60
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    
    @IBAction func onPlayVideo(_ sender: Any) {
        
        if post.video_url == "" { return }
        let url = URL(string: post.video_url)!
        let player = AVPlayer(url: url)
        let vc = AVPlayerViewController()
        vc.player = player

        self.present(vc, animated: true) {
            vc.player?.play()
        }
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickProfile(_ sender: Any) {
        self.slideMenuController()?.closeLeft()
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        let user = UserModel()
        user.id = post.user_id
        profileVC.user = user
        from = .other
        profileVC.modalPresentationStyle = .fullScreen
        self.present(profileVC, animated: false, completion: nil)
    }
    
    @IBAction func onClickMessage(_ sender: Any) {
        
        let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "MessagingVC") as! MessagingVC
        let user = UserModel()
        user.id = post.user_id
        user.photo_url = post.user_photo
        messageVC.target_user = user
        from = .other
        self.navigationController?.pushViewController(messageVC, animated: true)
    }
    
    @IBAction func onClickPlay(_ sender: Any) {
        
        if currentUser?.membership_type == -1 {
            self.showAlert(title: Const.APPNAME, message: "Upgrade to Date2Night to use this feature", okButtonTitle: Const.OK, cancelButtonTitle: Const.CANCEL) {
                
                let purchaseVC = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseVC") as! PurchaseVC
                self.navigationController?.pushViewController(purchaseVC, animated: true)
                
            }            
        }
        
        imageService.pickImage(from: self, allowEditing: false, source: .camera) {[weak self] attachements in
            self?.dismissVC()
            
            if attachements.count == 0 { return }
            self!.showLoadingView()
            ApiRequest.shared.upload_file(attachements) { (code, message, data) in
                self!.hideLoadingView()
                if code == Const.CODE_SUCCESS {
                    
                    let messageVC = self?.storyboard?.instantiateViewController(withIdentifier: "MessagingVC") as! MessagingVC
                    let user = UserModel()
                    user.id = self!.post.user_id
                    user.photo_url = self!.post.user_photo
                    messageVC.target_user = user
                    messageVC.attachData = data!
                    from = .video
                    self?.navigationController?.pushViewController(messageVC, animated: true)
                    
                } else {
                    self!.showAlert(message!)
                }
            }
        }
    }
    
    @IBAction func onClickMore(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "It's Spam", style: .destructive, handler: { (action) -> Void in
            self.reportPost()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "It's inappropriate", style: .destructive, handler: { (action) -> Void in
            self.reportPost()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func reportPost() {
        self.showLoadingView()
        ApiRequest.shared.report_post(post.id) { (code, message, value) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS {
                self.showAlert("This post is reported")
            } else {
                self.showAlert(message!)
            }
        }        
    }
}
