//
//  PostVC.swift
//  Date2Night
//
//  Created by Developer on 4/20/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import MobileCoreServices
import CoreLocation

enum StockImageType: Int {
    case activity = 0
    case dining
    case relax
}

class PostVC: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblMaleRed: UILabel!
    @IBOutlet weak var lblFemaleRed: UILabel!
    @IBOutlet weak var lblNospecRed: UILabel!
    @IBOutlet weak var tfLocation: UITextField!
    @IBOutlet weak var tfActivity: UITextField!
    @IBOutlet weak var tvSummary: UITextView!
    
    var collectionView:UICollectionView!
    let layout :UICollectionViewFlowLayout! = UICollectionViewFlowLayout()
    let picker: UIImagePickerController = UIImagePickerController()
    
    var selectedType = StockImageType.activity
    var relaxStockImages = [UIImage]()
    var activityStockImags = [UIImage]()
    var diningStockImages = [UIImage]()
    var selectedPreference = 0
    var itemWidth: CGFloat = 0.0
    var attachments = [(Data, String, String)]()
    var myAddress = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.picker.delegate = self
        picker.allowsEditing = true
        
        initView()
        updatePreferenceView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getLocationString()
    }
    
    func initView() {
        
        itemWidth = (self.view.size.width - 150) / 2.0
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        for i in 0..<10 {
            activityStockImags.append(UIImage(named: "activity_\(i+1)")!)
            diningStockImages.append(UIImage(named: "dining_\(i+1)")!)
            relaxStockImages.append(UIImage(named: "relax_\(i+1)")!)
        }
    }
    
    //MARK:- Private Methods
    func getLocationString() {
        self.myAddress = ""
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = latitude
        center.longitude = longitude

        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            
            {(placemarks, error) in
                
                if (error != nil) {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    return
                }
                let pm = placemarks! as [CLPlacemark]

                if pm.count > 0 {
                    let pm = placemarks![0]
                    print("county===", pm.country ?? "county")
                    print("administrativeArea===", pm.administrativeArea ?? "administrativeArea")
                    print("subAdministrativeArea===", pm.subAdministrativeArea ?? "subAdministrativeArea")
                    print("locality===", pm.locality ?? "locality")
                    print("subLocality===", pm.subLocality ?? "subLocality")
                    print("thoroughfare===", pm.thoroughfare ?? "thoroughfare")
                    print("postalCode===", pm.postalCode ?? "postalCode")
                    print("subThoroughfare===", pm.subThoroughfare ?? "subThoroughfare")
                    
                    //if pm.subLocality != nil {
                    //    self.myAddress = self.myAddress + pm.subLocality! + ", "
                    //}
                    //if pm.thoroughfare != nil {
                    //    self.myAddress = self.myAddress + pm.thoroughfare! + ", "
                    //}
                    if pm.locality != nil {
                        self.myAddress = self.myAddress + pm.locality! + ", "
                    } else {
                        self.myAddress = self.myAddress + pm.subLocality!
                    }
                    if pm.administrativeArea != nil {
                        self.myAddress = self.myAddress + pm.administrativeArea!// + ", "
                    } else {
                        self.myAddress = self.myAddress + pm.subAdministrativeArea!
                    }
                    //if pm.country != nil {
                    //    self.myAddress = self.myAddress + pm.country! + ", "
                    //}
                    //if pm.postalCode != nil {
                    //    self.myAddress = self.myAddress + pm.postalCode! + " "
                    //}
                    print("myAddress==", self.myAddress)
                    if self.myAddress != "" {
                        self.tfLocation.text = self.myAddress
                    }
                }
        })
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }    
    
    @IBAction func onAddPhoto(_ sender: Any) {
        
        selectMedia()
    }
    
    @IBAction func onChoosePreference(_ sender: UIButton) {
        
        selectedPreference = sender.tag
        updatePreferenceView()
    }
    
    @IBAction func OnClickSubmit(_ sender: Any) {
        
        if isValid() {
            submitPost()
        }
    }
    
    func isValid() -> Bool {
        
        if attachments.count == 0 {
            self.showAlert("Please add photo")
            return false
        }
        
        if tfLocation.text!.isEmpty {
            self.showAlert("Location is not found")
            return false
        }
        
        if tfActivity.text!.isEmpty {
            self.showAlert("Please input activity")
            return false
        }
        
        if tvSummary.text!.isEmpty {
            self.showAlert("Please input summary")
            return false
        }
        
        if !CommonUtils.isValidWords(tfActivity.text!) {
            self.showAlert("Your language of choice violates our Community Guidelines. Let’s keep D2N clean.")
            return false
        }
        
        if !CommonUtils.isValidWords(tvSummary.text!) {
            self.showAlert("Your language of choice violates our Community Guidelines. Let’s keep D2N clean.")
            return false
        }
        
        return true
    }
    
    func submitPost() {
        
        self.showLoadingView(vc: self)
        
        let post = PostModel()
        post.location = myAddress
        post.activity = tfActivity.text!
        post.preference = Const.PREFERENCES[selectedPreference]
        post.summary = tvSummary.text!
        
        ApiRequest.shared.add_post(post, attachments: attachments) { (result, message, value) in
            
            self.hideLoadingView()
            if result == Const.CODE_SUCCESS {
                self.dismissVC()
            } else {
                self.showAlert(message!)
            }
        }
    }
    
    func updatePreferenceView() {
        
        switch selectedPreference {
        case 0:
            lblMaleRed.isHidden = false
            lblFemaleRed.isHidden = true
            lblNospecRed.isHidden = true
        case 1:
            lblMaleRed.isHidden = true
            lblFemaleRed.isHidden = false
            lblNospecRed.isHidden = true
        default:
            lblMaleRed.isHidden = true
            lblFemaleRed.isHidden = true
            lblNospecRed.isHidden = false
        }
    }
    
    func selectMedia() {

        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)/* && UIImagePickerController.isSourceTypeAvailable(.camera)*/ {
        
            let actionSheet = UIAlertController(title: "MEDIA", message: "We suggest being creative", preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "Camera Roll", style: .default, handler: { (action) -> Void in
                self.openCameraRoll()
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Stock Images", style: .default, handler: { (action) -> Void in
                self.openStockImages()
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Video", style: .default, handler: { (action) -> Void in
                self.openVideo()
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    func openCameraRoll() {
        
        self.picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(self.picker, animated: true, completion: nil)
    }
    
    func openStockImages() {
        
        let actionSheet = UIAlertController(title: "CHOOSE YOUR EVENT", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Activity", style: .default, handler: { (action) -> Void in
            self.selectedType = .activity
            self.chooseStockImage()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Dining", style: .default, handler: { (action) -> Void in
            self.selectedType = .dining
            self.chooseStockImage()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Relax", style: .default, handler: { (action) -> Void in
            self.selectedType = .relax
            self.chooseStockImage()
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openVideo() {
        
        picker.sourceType = .camera
        picker.mediaTypes = [kUTTypeMovie as String]
        picker.videoMaximumDuration = TimeInterval(30)
        //picker.videoQuality = .typeHigh
        present(picker, animated: true, completion: nil)
    }
    
    func chooseStockImage() {
        var title = ""
        switch selectedType {
        case .activity:
            title = "Activity"
        case .dining:
            title = "Dining"
        default:
            title = "Relax"
        }
        
        let alert:UIAlertController=UIAlertController(title: title, message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertController.Style.actionSheet)

        let margin:CGFloat = 20.0
        let viewmargin:CGFloat = 20.0
        let rect = CGRect(x: viewmargin, y: margin, width: alert.view.bounds.size.width - viewmargin * 4.0, height: 200)
        let customView = UIView(frame: rect)
           //            customView.backgroundColor = .green
        customView.layer.masksToBounds = true
        customView.layer.cornerRadius = 5
        let rect1 = CGRect(x: viewmargin, y: margin, width: alert.view.bounds.size.width - viewmargin * 4.0 - 20, height: 200)
        collectionView = UICollectionView(frame: rect1, collectionViewLayout: layout)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "StockImageCell", bundle: .main), forCellWithReuseIdentifier: "StockImageCell")
        self.collectionView.backgroundColor = UIColor.clear
        collectionView.showsVerticalScrollIndicator = false
        customView.addSubview(self.collectionView)

        self.collectionView.topAnchor.constraint(equalTo: customView.topAnchor, constant: 0).isActive = true
        self.collectionView.leadingAnchor.constraint(equalTo: customView.leadingAnchor, constant: 0).isActive = true
        self.collectionView.trailingAnchor.constraint(equalTo: customView.trailingAnchor, constant: 0).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: customView.bottomAnchor, constant: 0).isActive = true
        
        alert.view.addSubview(customView)
            
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK:-ImagePickerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.editedImage] as? UIImage {        
            self.imvPhoto.image = image
            if let index = attachments.firstIndex(where: { $0.1 == "image/png"}) {
                attachments.remove(at: index)
            }
            attachments.append((image.jpegData(compressionQuality: 0.75)!, "image.png", "image/png"))
        } else if let selectedVideo = info[.mediaURL] as? URL {
            let thumbImage = createThumbnailImage(videopath: selectedVideo)
            self.imvPhoto.image = thumbImage
            if let index = attachments.firstIndex(where: { $0.1 == "image/png"}) {
                attachments.remove(at: index)
            }
            attachments.append((thumbImage!.jpegData(compressionQuality: 0.75)!, "image.png", "image/png"))
            if let index = attachments.firstIndex(where: { $0.1 == "video/mp4"}) {
                attachments.remove(at: index)
            }
            
            do {
                let videoData = try Data(contentsOf: selectedVideo, options: NSData.ReadingOptions())
                attachments.append((videoData, "vodeo.mp4", "video/mp4"))
            } catch {
                print(error)
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - colection view in actionsheet
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StockImageCell", for: indexPath) as! StockImageCell
        switch selectedType {
        case .activity:
            cell.imvPhoto.image = activityStockImags[indexPath.row]
        case .dining:
            cell.imvPhoto.image = diningStockImages[indexPath.row]
        default:
            cell.imvPhoto.image = relaxStockImages[indexPath.row]
        }
        cell.imvPhoto.layer.cornerRadius = 5
        cell.imvPhoto.layer.masksToBounds  = true
        return cell
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let cellSize = CGSize(width: itemWidth, height: itemWidth)
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var image = UIImage()
        switch selectedType {
        case .activity:
            image = activityStockImags[indexPath.row]
        case .dining:
            image = diningStockImages[indexPath.row]
        default:
            image = relaxStockImages[indexPath.row]
        }
        self.imvPhoto.image = image
        if let index = attachments.firstIndex(where: { $0.1 == "image/png"}) {
            attachments.remove(at: index)
        }
        attachments.append((image.jpegData(compressionQuality: 0.75)!, "image.png",  "image/png"))
        dismiss(animated: true, completion: nil)
    }
}

