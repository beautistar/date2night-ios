//
//  PreferenceVC.swift
//  Date2Night
//
//  Created by Developer on 4/17/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import AORangeSlider

class PreferenceVC: BaseViewController {

    @IBOutlet weak var lblLowAge: UILabel!
    @IBOutlet weak var lblHighAge: UILabel!
    @IBOutlet weak var ageSlider: AORangeSlider!
    @IBOutlet weak var vwFeMale: dropShadowNoRadiusView!
    @IBOutlet weak var vwMale: dropShadowNoRadiusView!
    
    var isMale: Bool!
    var isFemale: Bool!
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBarItem()
        setupCustomHeightSlider()
        ageSlider.lowValue = Double(min_age)
        ageSlider.highValue = Double(max_age)
        initGenderView()
        
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController!.navigationBar.titleTextAttributes = textAttributes
    }
    
    func initGenderView() {
        
        if preference == Const.PREFERENCES[0] {
            isMale = true
            isFemale = false
        } else if preference == Const.PREFERENCES[1] {
            isMale = false
            isFemale = true
        } else {
            isMale = true
            isFemale = true
        }
        
        updateGenderView()
    }
    
    @IBAction func onChangeGender(_ sender: UIButton) {
        
        if sender.tag == 0 {
            isMale = !isMale
        } else {
            isFemale = !isFemale
        }
        updateGenderView()
    }
    
    @IBAction func onClickContinue(_ sender: Any) {
        isFiltered = true
        min_age = Int(ageSlider.lowValue)
        max_age = Int(ageSlider.highValue)
        if isMale && !isFemale {
            preference = Const.PREFERENCES[0]
        } else if !isMale && isFemale {
            preference = Const.PREFERENCES[1]
        } else {
            preference = Const.PREFERENCES[2]
        }
        
        
        createMenuView()
    }
    
    func updateGenderView() {
        
        if isMale {
            vwMale.backgroundColor = Const.COLOR_LOGHT_MAIN
        } else {
            vwMale.backgroundColor = .white
        }
        
        if isFemale {
            vwFeMale.backgroundColor = Const.COLOR_LOGHT_MAIN
        } else {
            vwFeMale.backgroundColor = .white
        }
    }
    
    func setupCustomHeightSlider() {
        // magenta value bar and height is 5.0
        let trackImage = AORangeSlider.getImage(color: Const.COLOR_MAIN1!, size: CGSize(width: 1, height: 5))
        ageSlider.trackImage = trackImage

        // brown background bar and height is 5.0
        let trackBackgroundImage = AORangeSlider.getImage(color: .lightGray, size: CGSize(width: 1, height: 5))
        ageSlider.trackBackgroundImage = trackBackgroundImage
        ageSlider.setValue(low: 18, high: 100, animated: false)
        
        ageSlider.valuesChangedHandler = { [weak self] in
            guard let `self` = self else {
                return
            }            
            self.lblLowAge.text = "\(Int(self.ageSlider.lowValue))"
            self.lblHighAge.text = "\(Int(self.ageSlider.highValue))"
        }
    }
}
