//
//  UploadVideoVC.swift
//  Date2Night
//
//  Created by Developer on 4/23/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import MobileCoreServices

class UploadVideoVC: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var vwComment: UIView!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfDescription: UITextField!
    
    let picker: UIImagePickerController = UIImagePickerController()
    var attachments = [(Data, String, String)]()
    
    //MARK:- lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.picker.delegate = self
        picker.allowsEditing = true        
        
        vwComment.isHidden = true
        btnUpload.isHidden = true
    }

    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func onClickRecord(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            picker.sourceType = .camera
            picker.mediaTypes = [kUTTypeMovie as String]
            picker.videoMaximumDuration = TimeInterval(30)
            //picker.videoQuality = .typeHigh
            present(picker, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClickUpload(_ sender: Any) {
        
        if tfTitle.text!.isEmpty {
            self.showAlert("Please input title")
            return
        }
        
        if tfDescription.text!.isEmpty {
            self.showAlert("Please input description")
            return
        }
        
        if !CommonUtils.isValidWords(tfTitle.text!) {
            self.showAlert("Your language of choice violates our Community Guidelines. Let’s keep D2N clean.")
            return
        }
        
        if !CommonUtils.isValidWords(tfDescription.text!) {
            self.showAlert("Your language of choice violates our Community Guidelines. Let’s keep D2N clean.")
            return
        }
        
        uploadSmallPost()
    }
    
    func uploadSmallPost() {
        
        let small_post = SmallPostModel()
        small_post.title = tfTitle.text!
        small_post.description = tfDescription.text!
        
        self.showLoadingView(vc: self)
        ApiRequest.shared.add_small_post(small_post, attachments: attachments) { (code, message, value) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS {
                self.createMenuView()
            } else {
                self.showAlert(message!)                
            }
        }
    }
    
    //MARK:-ImagePickerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let selectedVideo = info[.mediaURL] as? URL {
            let thumbImage = createThumbnailImage(videopath: selectedVideo)
            if let index = attachments.firstIndex(where: { $0.1 == "image/png"}) {
                attachments.remove(at: index)
            }
            attachments.append((thumbImage!.jpegData(compressionQuality: 0.75)!, "image.png", "image/png"))
            if let index = attachments.firstIndex(where: { $0.1 == "video/mp4"}) {
                attachments.remove(at: index)
            }
            
            do {
                let videoData = try Data(contentsOf: selectedVideo, options: NSData.ReadingOptions())
                attachments.append((videoData, "vodeo.mp4", "video/mp4"))
            } catch {
                print(error)
            }
            
            vwComment.isHidden = false
            btnRecord.isHidden = true
            btnUpload.isHidden = false
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}
