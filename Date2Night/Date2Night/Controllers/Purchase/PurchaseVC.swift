//
//  PurchaseVC.swift
//  Date2Night
//
//  Created by Developer on 4/19/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class PurchaseVC: BaseViewController {

    var selectedPlan = 0
    @IBOutlet weak var vw12Month: UIView!
    @IBOutlet weak var vw6Month: UIView!
    @IBOutlet weak var vw1Month: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Purchase"
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController!.navigationBar.titleTextAttributes = textAttributes
        setNavigationBarItem()
        
        initPurchase()
    }
    
    func initPurchase() {
        
        IAPHandler.shared.fetchAvailableProducts()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            //self?.hideLoadingView()
            guard let strongSelf = self else{ return }
            if type == .purchased {
                let alertView = UIAlertController(title: Const.APPNAME, message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: Const.OK, style: .default, handler: { (alert) in
                    //TODO: success purchased
                    print("purchase succeed")
                    
                    //let userdefault = UserDefaults.standard
                    //userdefault.set(true, forKey: Const.KEY_ISPURCHASED)
                    self!.setMembership()
                    
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            
            } else if type == .restored {
                
                let alertView = UIAlertController(title: Const.APPNAME, message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: Const.OK, style: .default, handler: { (alert) in
                    //TODO: success purchased
                    print("restore succeed")
                    
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            } else {
                
                self?.showAlert("failed")
            }
        }
    }
    
    @IBAction func OnClikcPurchase(_ sender: UIButton) {
        switch sender.tag / 6 {
        case 0:
            vw1Month.backgroundColor = UIColor(named: "LightMainColor")
            vw6Month.backgroundColor = .white
            vw12Month.backgroundColor = .white
        case 1:
            vw6Month.backgroundColor = UIColor(named: "LightMainColor")
            vw1Month.backgroundColor = .white
            vw12Month.backgroundColor = .white
        default:
            vw12Month.backgroundColor = UIColor(named: "LightMainColor")
            vw6Month.backgroundColor = .white
            vw1Month.backgroundColor = .white
        }
        
        selectedPlan = sender.tag
    }
    
    
    @IBAction func onClickContinue(_ sender: Any) {
        if selectedPlan == 0 {
            self.showAlert("Please choose subscription plan")
            return
        }
        IAPHandler.shared.purchaseMyProduct(index: selectedPlan / 6)
    }
    
    func setMembership() {
        self.showLoadingView()
        ApiRequest.shared.set_membership(selectedPlan) { (code, message, value) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS {
                self.showAlert("You have succeeded to purchase membership plan")
            } else {
                self.showAlert(message!)
            }
        }
    }
    
    
}
