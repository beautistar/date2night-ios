//
//  SmallCircleCell.swift
//  Date2Night
//
//  Created by Developer on 4/19/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class SmallCircleCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblBadge: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    var entity: SmallPostModel! {
        
        didSet {
            imvPhoto.kf.indicatorType = .activity
            imvPhoto.kf.setImage(with: URL(string: entity.photo_url), placeholder: Const.PLACEHOLDER_IMAGE)
            lblTitle.text = entity.title
            if entity.highlighted == "yes" {
                imvPhoto.borderColor = UIColor.yellow
            } else {
                imvPhoto.borderColor = UIColor(named: "BorderColor")
            }
        }
    }
}
