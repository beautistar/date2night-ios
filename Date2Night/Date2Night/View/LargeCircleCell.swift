//
//  LargeCircleCell.swift
//  Date2Night
//
//  Created by Developer on 4/19/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class LargeCircleCell: UICollectionViewCell {
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblViewed: UILabel!
    
    override class func awakeFromNib() {
        
    }
    
    var entity: PostModel! {
        
        didSet {
            imvPhoto.kf.indicatorType = .activity
            imvPhoto.kf.setImage(with: URL(string: entity.photo_url), placeholder: Const.PLACEHOLDER_IMAGE)
            lblTitle.text = entity.activity
            lblViewed.text = "\(entity.view_count)"
            if entity.highlighted == "yes" {
                imvPhoto.borderColor = UIColor.yellow
            } else {
                imvPhoto.borderColor = UIColor(named: "BorderColor")
            }
        }
    }
}
