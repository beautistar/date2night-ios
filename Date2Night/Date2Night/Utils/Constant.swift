//
//  Constant.swift
//  Ride
//
//  Created by Yin on 2018/9/6.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation
import UIKit


class Const {

    static let APPNAME                      = "Date2Night"
    static let OK                           = "OK"
    static let CANCEL                       = "Cancel"
    static let NO                           = "No"
    static let YES                          = "Yes"
    // Array
    static let PREFERENCES                  = ["Male", "Female", "No Specified"]
    static let BANNED_WORDS                 = ["Sex", "Intercourse", "Fuck", "Pussy", "Dick", "Head", "Dome", "Anal", "Oral", "Clit", "Swallow", "Lick", "Threesome", "Tits", "Breast", "Molly", "Drugs", "Guns", "Ass", "Weed", "Fagot", "Nigger", "Nigga", "Bitch", "Shit", "Slut", "Whore", "Hoe", "Suck", "Penis", "Gay", "Porn"]
    
    //Color
    static let COLOR_MAIN                   = UIColor(named: "PrimaryColor")
    static let COLOR_BAR                    = UIColor(named: "NavigationBarColor")
    static let COLOR_MAIN1                  = UIColor(named: "MainColor")
    static let COLOR_LOGHT_MAIN             = UIColor(named: "LightMainColor")
    
    static let PLACEHOLDER_IMAGE            = UIImage(named: "placeholder")
    static let PLACEHOLDER_PROFILE          = UIImage(named: "profile")
    
    static let DISTANCE_MAX                 = 30000000/// mile
    static let PRICE_MAX                    = 10000000000000
    
    //error messages
    static let CHECK_NAME_EMPTY             = "Please enter name"
    static let CHECK_EMAIL_EMPTY            = "Please enter your email address"
    static let CHECK_VAILD_EMAIL            = "Please enter valid email"
    static let CHECK_EMPTY_PHONE            = "Please enter your phone number"
    static let CHECK_PASSWORD               = "Please enter password"
    static let CHECK_CONFIRM_PASSWORD       = "Please enter confirm password"
    static let CHECK_PASSWORD_MATCH         = "Password does not match"

    static let MSG_SUCCESS                  = "Success"
    static let MSG_FAIL                     = "Fail"
    static let ERROR_CONNECT                = "Failed to server connection"
    static let MSG_SOMETHING_WRONG          = "Something went wrong"
    
    // Response parameters
    static let RES_MESSAGE                  = "message"

    
    // Key
    static let KEY_USERID                   = "k_id"
    static let KEY_NAME                     = "k_name"
    static let KEY_TOKEN                    = "k_token"
    static let KEY_FBID                     = "k_fbid"
    static let KEY_PHONE                    = "k_phone"
    static let KEY_TYPE                     = "k_type"
    static let KEY_PHOTO                    = "k_photo"
    
    // Result code
    static let RESULT_CODE                  = "result_code"
    static let CODE_201                     = 201
    static let CODE_SUCCESS                 = 200
    static let CODE_202                     = 202
    static let CODE_203                     = 203
    static let CODE_FAIL                    = 400
}
