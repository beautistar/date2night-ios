//
//  CommonUtils.swift
//  Pixil
//
//  Created by Developer on 8/19/19.
//  Copyright © 2019 Ankhbayar Batbaatar. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit

enum From {
    case signup
    case main
    case message
    case preference
    case purchase
    case other
    case profile
    case mypost
    case video
}

var ScreenRect : CGRect {
    return UIScreen.main.bounds
}

var from = From.main
var isFiltered = false
var min_age = 18
var max_age = 100
var preference = "No Specified"
var latitude = 0.0
var longitude = 0.0

var currentUser : UserModel? {
    didSet {
        if let user = currentUser {
            UserDefaults.standard.set(user.name, forKey: Const.KEY_NAME)
            UserDefaults.standard.set(user.id, forKey: Const.KEY_USERID)
            UserDefaults.standard.set(user.fb_id, forKey: Const.KEY_FBID)
            UserDefaults.standard.set(user.phone, forKey: Const.KEY_PHONE)
            UserDefaults.standard.set(user.type, forKey: Const.KEY_TYPE)
            UserDefaults.standard.set(user.phone, forKey: Const.KEY_PHOTO)
        }
    }
}

class CommonUtils {
    
    static func isValidEmail(_ email: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    static func isValidWords(_ word: String) -> Bool {
        for bannedWord in Const.BANNED_WORDS {
            if word.lowercased().contains(bannedWord.lowercased()) {
                return false
            }
        }
        return true
    }
}
// "2020-12-20" to "20 Dec 2020"
func formatedDate(_ dateString: String) -> String {
    
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "dd MMM HH:mm:a"

    if let date = dateFormatterGet.date(from: dateString) {
        return dateFormatterPrint.string(from: date)
    } else {
       return ""
    }
}

// "2020-12-20" to "20 Dec 2020"
func expireDate(_ dateString: String, day: Int) -> String {
    
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "yyyy-MM-dd"
    if let curDate = dateFormatterGet.date(from: dateString) {
        let interval = TimeInterval(60 * 60 * 24 * day)
        let newDate = curDate.addingTimeInterval(interval)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        //if let date = dateFormatterGet.date(from: newDate) {
        return dateFormatterPrint.string(from: newDate)
    } else {
       return ""
    }
}

func getMessageTimeFromTimeStamp(_ timeStamp: Int64) -> String {
    
    let date = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
    let dateFormatter = DateFormatter()
    //dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
    dateFormatter.locale = NSLocale.current
    dateFormatter.dateFormat = "MM/dd h:mm a" //Specify your format that you want
    dateFormatter.amSymbol = "AM"
    dateFormatter.pmSymbol = "PM"
    let strDate = dateFormatter.string(from: date)
    return strDate
}

func getDiffSeconds(_ dateString: String) -> Int {
    
    let timeformatter = DateFormatter()
    //timeformatter.locale = NSLocale.current
    timeformatter.timeZone = TimeZone(identifier: "UTC")
    timeformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    guard let time1 = timeformatter.date(from: dateString) else { return -1 }
    let interval = time1.timeIntervalSince(Date())
    //let day = interval / 3600 / 24
    //let hour = interval / 3600
    //let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
    //let intervalInt = Int(interval)
    //print("diff time===", intervalInt)
    //return "\(intervalInt < 0 ? "-" : "+") \(Int(day)) Days \(Int(hour)) Hours \(Int(minute)) Minutes"
    
    return Int(interval)
    
}


//MARK:- saveToFile
// save image to a file (Documents/LLSeller/temp.png)
//MARK:-
func saveToFile(image: UIImage!, filePath: String!, fileName: String) -> String! {
    
    let outputFileName = fileName
    
    let outputImage = resizeImage(srcImage: image)
    
    let fileManager = FileManager.default
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    var documentDirectory: NSString! = paths[0] as NSString?
    
    // current document directory
    fileManager.changeCurrentDirectoryPath(documentDirectory as String)
    
    do {
        try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
    } catch let error as NSError {
        print(error.localizedDescription)
    }
    
    documentDirectory = documentDirectory.appendingPathComponent(filePath) as NSString?
    let savedFilePath = documentDirectory.appendingPathComponent(outputFileName)
    
    // if the file exists already, delete and write, else if create filePath
    if (fileManager.fileExists(atPath: savedFilePath)) {
        do {
            try fileManager.removeItem(atPath: savedFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    } else {
        fileManager.createFile(atPath: savedFilePath, contents: nil, attributes: nil)
    }
    
    if let data = outputImage.pngData() {
        
        do {
            try data.write(to:URL(fileURLWithPath:savedFilePath), options:.atomic)
        } catch {
            print(error)
        }
    }
    return savedFilePath
}

func resizeImage(srcImage: UIImage) -> UIImage {

    if (srcImage.size.width >= srcImage.size.height) {

        return srcImage.resize(toTargetSize: CGSize(width: 256, height: 330))
    } else {

        return srcImage.resize(toTargetSize: CGSize(width: 256, height: 330))
    }
}

//MARK:- Create Thumbnail Image
//MARK:-
// Get Thumbnail Image from URL
func createThumbnailImage(videopath: URL) -> UIImage? {
    let asset = AVURLAsset(url: videopath)
    let generator = AVAssetImageGenerator(asset: asset)
    generator.appliesPreferredTrackTransform = true
    let timestamp = CMTime(seconds: 2, preferredTimescale: 60)
    if let imageRef = try? generator.copyCGImage(at: timestamp, actualTime: nil) {
        return UIImage(cgImage: imageRef)
    } else {
        return nil
    }
}
