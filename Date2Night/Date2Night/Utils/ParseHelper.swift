//
//  ParseHelper.swift
//  Transform
//
//  Created by Yin on 26/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//


import Foundation
import SwiftyJSON

class ParseHelper {
    
    static func parseUser(_ userObject: JSON) -> UserModel {
        
        let user = UserModel()
        
        user.id = userObject["id"].intValue
        user.name = userObject["name"].stringValue
        user.fb_id = userObject["social_id"].intValue
        user.phone = userObject["phone"].stringValue
        user.gender = userObject["gender"].stringValue
        user.age = userObject["age"].intValue
        user.orientation = userObject["orientation"].stringValue
        user.instagram = userObject["instagram"].stringValue
        user.snapchat = userObject["snapchat"].stringValue
        user.photo_url = userObject["photo_url"].stringValue
        user.photo_url1 = userObject["photo_url1"].stringValue
        user.photo_url2 = userObject["photo_url2"].stringValue
        user.type = userObject["login_type"].stringValue
        user.noti_count = userObject["noti_count"].intValue
        user.membership_type = userObject["membership_type"].intValue
        user.membership_date = userObject["membership_date"].stringValue
        if userObject["block"].exists() {
            user.block = userObject["block"].intValue
        }
        return user
    }
    
    static func parsePost(_ object: JSON) -> PostModel {

        let model = PostModel()

        model.id = object["id"].intValue
        model.user_id = object["user_id"].intValue
        model.user_photo = object["user_photo"].stringValue
        model.location = object["location"].stringValue
        model.latitude = object["latitude"].doubleValue
        model.longitude = object["longitude"].doubleValue
        model.activity = object["activity"].stringValue.decodeEmoji
        model.preference = object["preference"].stringValue
        model.summary = object["summary"].stringValue.decodeEmoji
        model.photo_url = object["photo_url"].stringValue
        model.video_url = object["video_url"].stringValue
        model.reported_count = object["reported_count"].intValue
        model.view_count = object["view_count"].intValue
        model.highlighted = object["highlighted"].stringValue
        model.created_at = object["created_at"].stringValue
        
        return model        
    }
    
    static func parseSmallPost(_ object: JSON) -> SmallPostModel {

        let model = SmallPostModel()

        model.id = object["id"].intValue
        model.user_id = object["user_id"].intValue
        model.user_photo = object["user_photo"].stringValue
        model.latitude = object["latitude"].doubleValue
        model.longitude = object["longitude"].doubleValue
        model.title = object["title"].stringValue.decodeEmoji
        model.description = object["description"].stringValue.decodeEmoji
        model.photo_url = object["photo_url"].stringValue
        model.video_url = object["video_url"].stringValue
        model.reported_count = object["reported_count"].intValue
        model.highlighted = object["highlighted"].stringValue
        model.created_at = object["created_at"].stringValue
        
        return model
    }

}
