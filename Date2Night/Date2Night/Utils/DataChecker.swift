//
//  DataChecker.swift
//  Wedding
//
//  Created by Shiv on 09/02/18.
//  Copyright © 2018 Shiv. All rights reserved.
//

import UIKit

class DataChecker: NSObject {

//    class func `init`(dict: [String: Any], key: String) -> String {
//        
//        var str : String = ""
//        
//        if let nm = dict[key] as? NSNumber {
//            str = nm.stringValue
//        } else if let int = dict[key] as? Int {
//            str = String(format: "%d", int)
//        } else if let st = dict[key] as? String {
//            str = st
//        }
//        
//        return str
//    }
    class func getValidationstring(dict: [String: Any], key: String) -> String {
        var str : String = ""
        
        if let nm = dict[key] as? NSNumber {
            str = nm.stringValue
        } else if let int = dict[key] as? Int {
            str = String(format: "%d", int)
        } else if let st = dict[key] as? String {
            str = st
        }
        
        return str
    }
}
