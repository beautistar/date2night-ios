//  MIT License

//  Copyright (c) 2019 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit
import Photos
import MobileCoreServices

class ImagePickerService: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
    var attachments = [(Data, String, String)]()
        
    private lazy var picker: UIImagePickerController = {
            let picker = UIImagePickerController()
            picker.delegate = self
            return picker
  
    }()
    var completionBlock: CompletionObject<[(Data, String, String)]>?
  
    func pickImage(from vc: UIViewController, allowEditing: Bool = true, source: UIImagePickerController.SourceType? = nil, completion: CompletionObject<[(Data, String, String)]>?) {
        completionBlock = completion
        picker.allowsEditing = allowEditing
        picker.mediaTypes = [kUTTypeMovie as String]
        picker.videoMaximumDuration = TimeInterval(30)
        guard let source = source else {
            let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            sheet.view.tintColor = Const.COLOR_MAIN
            let cameraAction = UIAlertAction(title: "Camera", style: .default) {[weak self] _ in
                guard let weakSelf = self else { return }
                weakSelf.picker.sourceType = .camera
                vc.present(weakSelf.picker, animated: true, completion: nil)
            }
            let photoAction = UIAlertAction(title: "Gallery", style: .default) {[weak self] _ in
                guard let weakSelf = self else { return }
                weakSelf.picker.sourceType = .photoLibrary
                vc.present(weakSelf.picker, animated: true, completion: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)      
            sheet.addAction(cameraAction)
            sheet.addAction(photoAction)
            sheet.addAction(cancelAction)
            vc.present(sheet, animated: true, completion: nil)
            return
        }
        picker.sourceType = source
        vc.present(picker, animated: true, completion: nil)
  }
  

//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        picker.dismiss(animated: true) {
//            if let image = info[.editedImage] as? UIImage {
//                self.completionBlock?(image.fixOrientation())
//                return
//            }
//            if let image = info[.originalImage] as? UIImage {
//                self.completionBlock?(image.fixOrientation())
//            }
//        }
//    }
    
    //MARK:-ImagePickerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let selectedVideo = info[.mediaURL] as? URL {
            let thumbImage = createThumbnailImage(videopath: selectedVideo)
            if let index = attachments.firstIndex(where: { $0.1 == "image/png"}) {
                attachments.remove(at: index)
            }
            attachments.append((thumbImage!.jpegData(compressionQuality: 0.75)!, "image.png", "image/png"))
            if let index = attachments.firstIndex(where: { $0.1 == "video/mp4"}) {
                attachments.remove(at: index)
            }
            
            do {
                let videoData = try Data(contentsOf: selectedVideo, options: NSData.ReadingOptions())
                attachments.append((videoData, "video.mp4", "video/mp4"))
            } catch {
                print(error)
            }
            
            self.completionBlock?(attachments)

        }
    }
}
