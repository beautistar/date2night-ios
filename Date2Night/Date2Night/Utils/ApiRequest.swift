//
//  ApiRequest.swift
//  Pixil
//
//  Created by Developer on 8/19/19.
//  Copyright © 2019 Ankhbayar Batbaatar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiRequest {
    
    // MARK: - Request URLS
    static let BASE_URL                             = "http://date2nightapp.com/"
    static let SERVER_URL                           = BASE_URL + "api/"
    

    let REQ_FB_LOGIN                         = SERVER_URL + "user/social_login"
    let REQ_SEND_OTP                         = SERVER_URL + "user/send_otp"
    let REQ_PHONE_LOGIN                      = SERVER_URL + "user/phone_login"
    let REQ_GETUSER                          = SERVER_URL + "user/get_user"
    let REQ_REGISTER_TOKEN                   = SERVER_URL + "user/register_token"
    let REQ_EDIT_USER                        = SERVER_URL + "user/edit_user"
    let REQ_UPLOAD_PHOTO                     = SERVER_URL + "user/upload_photo"
    let REQ_ADD_INBOX                        = SERVER_URL + "user/add_inbox"
    let REQ_GET_INBOX                        = SERVER_URL + "user/get_inbox_user"
    let REQ_SET_MEMBERSHIP                   = SERVER_URL + "user/set_membership"
    

    
    
    static let shared = ApiRequest()
    
    // MARK: - APIs
    //  MARK: - Private Helper for HTTP requst
    ///
    /// - parameter method:             HTTP Method(get, post, put, delete)
    /// - parameter url:                Call URL
    /// - parameter parameters:         Parameters
    /// - parameter completion:         Function to call when the response is obtained (Status: Bool, Message:  String?, Response: [String; Any]?
    fileprivate func request(_ method: HTTPMethod = .post, url: String, parameters: [String: Any]? = nil, completion: @escaping (Int, String?, Any?) -> Void) {
        AF.request(url, method: method, parameters: parameters)
        .validate()
        .responseJSON { response in
            guard response.error == nil else {
               // got an error in response
                completion(Const.CODE_FAIL, (response.error)?.localizedDescription, nil)
                return
            }

            // check response valud is valid
            guard let value = response.value as? [String: Any] else {
                completion(Const.CODE_FAIL, "Internal server error is occured.", nil)
               return
            }
           
            // TODO:
            // Do common parse from response, extra will have main data from server
            // This represents common response format of app API server
            if  let result = value["result_code"] as? Int {
                let message = value["message"] as? String
                completion(result, message, value)
                
            } else {
                completion(Const.CODE_FAIL, "Internal server error is occured.", nil)
            }
        }
    }
    
    //  MARK: - Private Helper for multipartFormData upload
    ///
    /// - parameter to:                         URL to upload
    /// - parameter multipartFormData:          FormData to upload
    /// - parameter completion:                 Function to call when the response is obtained (Status: Bool, Message:  String?, Response: [String; Any]?
    fileprivate func multipartFormDataUpload(_ multipartFormData: @escaping (MultipartFormData) -> Void, url: String, completion: @escaping (Int, String?, Any?) -> Void) {
        AF.upload(multipartFormData: multipartFormData, to: url)
        .validate()
        .responseJSON { response in            
            guard response.error == nil else {
               // got an error in response
                completion(Const.CODE_FAIL, (response.error)?.localizedDescription, nil)
                return
            }

            // check response valud is valid
            guard let value = response.value as? [String: Any] else {
                completion(Const.CODE_FAIL, "Internal server error is occured.", nil)
               return
            }
            
            // TODO:
            // Do common parse from response, extra will have main data from server
            // This represents common response format of app API server
            if  let result = value["result_code"] as? Int {
                let message = value["message"] as? String
                completion(result, message, value)
                
            } else {
                completion(Const.CODE_FAIL, "Internal server error is occured.", nil)
            }
        }
    }
    
    func fb_login(_ user: UserModel, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params = ["name": user.name,
                      "social_id": user.fb_id,
                      "photo_url": user.photo_url] as [String : Any]
        
        request(.post, url: REQ_FB_LOGIN, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS, let value = value {
                
                let user = ParseHelper.parseUser(JSON(value)["user_data"])
                currentUser = user
                completion(result, message, user)
            } else if let value = value {
                let id = JSON(value)["id"].intValue
                user.id = id
                currentUser = user
                completion(result, message, id)
            }
        }
    }
    
    func send_otp(phone: String, completion: @escaping (Int, String, Any?) -> () ) {
        
        let params = ["phone" : phone] as [String: Any]
        
        request(.post, url: REQ_SEND_OTP, parameters: params) { (result, message, value) in
            
            if result == Const.CODE_FAIL {
                completion(result, message!, nil)
            
            } else if result == Const.CODE_SUCCESS, let value = value {
                
                let pincode = JSON(value)["pincode"].stringValue
                completion(result, message!, pincode)
            }
        }
    }
    
    func phone_login(_ phone: String, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params = ["phone": phone] as [String : Any]
        
        request(.post, url: REQ_PHONE_LOGIN, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS, let value = value {
                
                let user = ParseHelper.parseUser(JSON(value)["user_data"])
                currentUser = user
                completion(result, message, user)
            } else if let value = value {
                let id = JSON(value)["id"].intValue
                let user = UserModel()
                user.id = id
                user.phone = phone
                user.type = "phone"
                currentUser = user
                completion(result, message, id)
            }
        }
    }
    
    func get_user(_ user_id: String, completion: @escaping (Int, String?, UserModel?) -> ()) {
        
        let params = ["user_id": user_id] as [String : Any]
        
        request(.post, url: REQ_GETUSER, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS, let value = value {                
                let user = ParseHelper.parseUser(JSON(value)["user_data"])
                
                completion(result, message, user)
            }
        }
    }
    
    func edit_user(_ user: UserModel, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params = ["user_id": user.id,
                      "name": user.name,
                      "gender": user.gender,
                      "age": user.age,
                      "orientation": user.orientation,
                      "instagram": user.instagram,
                      "snapchat": user.snapchat] as [String : Any]
        
        request(.post, url: REQ_EDIT_USER, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                currentUser = user
                completion(result, message, nil)
            } else {                
                completion(result, message, nil)
            }
        }
    }
    
    func upload_photo(attachments: [(Data, String)], completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] = ["user_id": "\(currentUser!.id)"]
        
        multipartFormDataUpload({ (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append(Data(value.utf8), withName: key)
            }
            for attachment in attachments {
                multipartFormData.append(attachment.0, withName: attachment.1, fileName: "\(attachment.1).png", mimeType: "image/png")
            }
        }, url: REQ_UPLOAD_PHOTO) { (result, message, value) in
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                let json = JSON(value!)
                if json["photo_url"].exists() {
                    currentUser!.photo_url = json["photo_url"].stringValue
                }
                if json["photo_url1"].exists() {
                    currentUser!.photo_url1 = json["photo_url1"].stringValue
                }
                if json["photo_url2"].exists() {
                    currentUser!.photo_url2 = json["photo_url2"].stringValue
                }
                completion(result, message, nil)
            } else {
                completion(result, message, nil)
            }
        }
    }
    
    func add_post(_ post: PostModel, attachments: [(Data, String, String)], completion: @escaping (Int, String?, Any?) -> () ) {
        
        let url = ApiRequest.SERVER_URL + "post/add_post"
        var highlighted = "no"
        if currentUser!.membership_type != -1 {
            highlighted = "yes"
        }
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)",
            "user_photo": "\(currentUser!.photo_url)",
            "highlighted": "\(highlighted)",
            "location": post.location,
            "latitude": "\(latitude)",
            "longitude": "\(longitude)",
            "activity": post.activity.encodeEmoji,
            "preference": post.preference,
            "summary": post.summary.encodeEmoji
        ]
        
        multipartFormDataUpload({ (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append(Data((value).utf8), withName: key)
            }
            for i in 0..<attachments.count {
                multipartFormData.append(attachments[i].0, withName: "post[\(i)]", fileName: attachments[i].1, mimeType: attachments[i].2)
            }
        }, url: url) { (result, message, value) in
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func add_small_post(_ small_post: SmallPostModel, attachments: [(Data, String, String)], completion: @escaping (Int, String?, Any?) -> () ) {
        
        let url = ApiRequest.SERVER_URL + "post/add_small_post"
        var highlighted = "no"
        if currentUser!.membership_type != -1 {
            highlighted = "yes"
        }
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)",
            "user_photo": "\(currentUser!.photo_url)",
            "highlighted": "\(highlighted)",
            "title": small_post.title.encodeEmoji,
            "description": small_post.description.encodeEmoji,
            "latitude": "\(latitude)",
            "longitude": "\(longitude)"
        ]
        
        multipartFormDataUpload({ (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append(Data((value).utf8), withName: key)
            }
            for i in 0..<attachments.count {
                multipartFormData.append(attachments[i].0, withName: "post[\(i)]", fileName: attachments[i].1, mimeType: attachments[i].2)
            }
        }, url: url) { (result, message, value) in
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func get_post(completion: @escaping (Int, String?, [PostModel]?) -> () ) {
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)",
            "latitude": "\(latitude)",
            "longitude": "\(longitude)"]
        
        let url = ApiRequest.SERVER_URL + "post/get_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS, let value = value {
                var posts = [PostModel]()
                let list = JSON(value)["post_list"].arrayValue
                for one in list{
                    posts.append(ParseHelper.parsePost(one))
                }
                
                completion(result, message, posts)
            } else {
                completion(result, message, nil)
            }
        }
    }
    
    func get_small_post(completion: @escaping (Int, String?, [SmallPostModel]?) -> () ) {
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)",
            "latitude": "\(latitude)",
            "longitude": "\(longitude)"]
        
        let url = ApiRequest.SERVER_URL + "post/get_small_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS, let value = value {
                var posts = [SmallPostModel]()
                let list = JSON(value)["post_list"].arrayValue
                for one in list{
                    posts.append(ParseHelper.parseSmallPost(one))
                }
                completion(result, message, posts)
            } else {
                completion(result, message, nil)
            }
        }
    }
    
    func get_my_post(completion: @escaping (Int, String?, [PostModel]?) -> () ) {
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)"]
        
        let url = ApiRequest.SERVER_URL + "post/get_my_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS, let value = value {
                var posts = [PostModel]()
                let list = JSON(value)["post_list"].arrayValue
                for one in list{
                    posts.append(ParseHelper.parsePost(one))
                }                
                completion(result, message, posts)
            } else {
                completion(result, message, nil)
            }
        }
    }
    
    func get_my_small_post(completion: @escaping (Int, String?, [SmallPostModel]?) -> () ) {
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)"]
        
        let url = ApiRequest.SERVER_URL + "post/get_my_small_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS, let value = value {
                var posts = [SmallPostModel]()
                let list = JSON(value)["post_list"].arrayValue
                for one in list{
                    posts.append(ParseHelper.parseSmallPost(one))
                }
                completion(result, message, posts)
            } else {
                completion(result, message, nil)
            }
        }
    }
    
    func delete_post(_ post_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] = [
            "post_id": "\(post_id)"]
        
        let url = ApiRequest.SERVER_URL + "post/delete_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            } else {
                completion(result, message, nil)
            }
        }
    }
    
    func delete_small_post(_ post_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] = [
            "post_id": "\(post_id)"]
        
        let url = ApiRequest.SERVER_URL + "post/delete_small_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            } else {
                completion(result, message, nil)
            }
        }
    }
    
    func filter_post(completion: @escaping (Int, String?, [PostModel]?) -> () ) {
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)",
            "gender": "\(currentUser!.gender)",
            "min_age": "\(min_age)",
            "max_age": "\(max_age)",
            "preference": preference,
            "latitude": "\(latitude)",
            "longitude": "\(longitude)"]
        
        let url = ApiRequest.SERVER_URL + "post/filter_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS, let value = value {
                var posts = [PostModel]()
                let list = JSON(value)["post_list"].arrayValue
                for one in list{
                    posts.append(ParseHelper.parsePost(one))
                }
                completion(result, message, posts)
            } else {
                completion(result, message, nil)
            }
        }
    }
    
    func view_post(_ post_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)",
            "post_id": "\(post_id)"]
        
        let url = ApiRequest.SERVER_URL + "post/view_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func report_post(_ post_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)",
            "post_id": "\(post_id)"]
        
        let url = ApiRequest.SERVER_URL + "post/report_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func report_small_post(_ post_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)",
            "post_id": "\(post_id)"]
        
        let url = ApiRequest.SERVER_URL + "post/report_small_post"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func add_inbox(_ target_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] =
            ["user_id": "\(currentUser!.id)",
             "target_id": "\(target_id)"]
        
        let url = ApiRequest.SERVER_URL + "user/add_inbox"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func delete_inbox(_ target_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] =
            ["user_id": "\(currentUser!.id)",
             "target_id": "\(target_id)"]
        
        let url = ApiRequest.SERVER_URL + "user/delete_inbox"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func get_inbox_user(completion: @escaping (Int, String?, [UserModel]?) -> () ) {
        
        let params: [String: String] = ["user_id": "\(currentUser!.id)"]
        
        let url = ApiRequest.SERVER_URL + "user/get_inbox_user"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
            
            
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                let json = JSON(value!)
                let userObject = json["user_list"].arrayValue
                var users = [UserModel]()
                for one in userObject {
                    let user = ParseHelper.parseUser(one)
                    users.append(user)
                }
                
                completion(result, message, users)
            }
        }
    }
    
    func add_noti_count(_ target_id: Int, message: String, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] =
                ["user_id": "\(currentUser!.id)",
                "target_id": "\(target_id)",
                "message": message]
        
        let url = ApiRequest.SERVER_URL + "user/add_noti_count"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func get_noti_count(completion: @escaping (Int, String?, Int) -> ()) {
        
        let params: [String: String] =
            ["user_id": "\(currentUser!.id)"]
        
        let url = ApiRequest.SERVER_URL + "user/get_noti_count"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, 0)
            } else if result == Const.CODE_SUCCESS {
                let count = JSON(value!)["count"].intValue
                completion(result, message, count)
            }
        }
    }
    
    func delete_noti_count(_ target_id: Int, count: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] =
            ["user_id": "\(currentUser!.id)",
             "target_id": "\(target_id)",
                "count": "\(count)"]
        
        let url = ApiRequest.SERVER_URL + "user/delete_noti_count"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func register_token(_ token: String, completion: @escaping (String) -> () ) {
        
        let params : [String: String] = ["user_id": "\(currentUser!.id)", "token": token]
        let url = ApiRequest.SERVER_URL + "user/register_token"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
            if result == Const.CODE_FAIL {
                completion(message!)
            } else if result == Const.CODE_SUCCESS {
                completion(message!)
            }
        }
    }
    
    func set_membership(_ membership_type: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] = [
            "user_id": "\(currentUser!.id)",
            "membership_type": "\(membership_type)"]
        
        let url = ApiRequest.SERVER_URL + "user/set_membership"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func block_user(_ target_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] =
            ["user_id": "\(currentUser!.id)",
             "target_id": "\(target_id)"]
        
        let url = ApiRequest.SERVER_URL + "user/block_user"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func unblock_user(_ target_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] =
            ["user_id": "\(currentUser!.id)",
             "target_id": "\(target_id)"]
        
        let url = ApiRequest.SERVER_URL + "user/unblock_user"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                completion(result, message, nil)
            }
        }
    }
    
    func get_block(_ target_id: Int, completion: @escaping (Int, String?, Any?) -> ()) {
        
        let params: [String: String] =
            ["user_id": "\(currentUser!.id)",
             "target_id": "\(target_id)"]
        
        let url = ApiRequest.SERVER_URL + "user/get_block"
        
        request(.post, url: url, parameters: params) { (result, message, value) in
           
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                let block = JSON(value!)["block_user"]["block"].intValue
                completion(result, message, block)
            }
        }
    }
    
    func upload_file(_ attachments: [(Data, String, String)], completion: @escaping (Int, String?, [String]?) -> () ) {
        
        let url = ApiRequest.SERVER_URL + "post/upload_file"
        
        multipartFormDataUpload({ (multipartFormData) in
            for i in 0..<attachments.count {
                multipartFormData.append(attachments[i].0, withName: "file[\(i)]", fileName: attachments[i].1, mimeType: attachments[i].2)
            }
        }, url: url) { (result, message, value) in
            if result == Const.CODE_FAIL {
                completion(result, message, nil)
            } else if result == Const.CODE_SUCCESS {
                
                let photoUrl = JSON(value!)["photo_url"].stringValue
                let videoUrl = JSON(value!)["video_url"].stringValue
                completion(result, message, [photoUrl, videoUrl])
            }
        }
    }
}
