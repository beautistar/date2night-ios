//
//  PostModel.swift
//  Date2Night
//
//  Created by Developer on 4/28/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class PostModel {
    
    var id = 0
    var user_id = 0
    var user_photo = ""
    var location = ""
    var latitude = 0.0
    var longitude = 0.0
    var activity = ""
    var preference = ""
    var summary = ""
    var photo_url = ""
    var video_url = ""
    var view_count = 0
    var highlighted = ""
    var reported_count = 0
    var created_at = ""

}
