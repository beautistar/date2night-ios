//
//  MessageModel.swift
//  Pixil
//
//  Created by Developer on 10/4/19.
//  Copyright © 2019 Ankhbayar Batbaatar. All rights reserved.
//

import UIKit

class MessageModel {
    
    var id: String = ""
    var receiver_id: String = ""
    var sender_id: String = ""
    var message: String = ""
    var video_url: String = ""
    var content_type = "text"
    var time: Int64 = 0
    var receiver_img: String = ""
    var sender_img: String = ""
    var receiver_name: String = ""
    var sender_name: String = ""
    
    class func parseMessageData(ary: NSArray) -> NSMutableArray {
        
        let muary = NSMutableArray()
        
        for index in 0 ..< ary.count {
            
            let dict = ary.object(at: index) as! [String: Any]
            
            let objList = MessageModel()
            objList.message = DataChecker .getValidationstring(dict: dict, key: "message")
            objList.video_url = DataChecker .getValidationstring(dict: dict, key: "video_url")
            objList.content_type = DataChecker .getValidationstring(dict: dict, key: "content_type")
            objList.receiver_id = DataChecker .getValidationstring(dict: dict, key: "receiver_id")
            objList.receiver_img = DataChecker .getValidationstring(dict: dict, key: "receiver_img")
            objList.sender_id = DataChecker .getValidationstring(dict: dict, key: "sender_id")
            objList.sender_img = DataChecker .getValidationstring(dict: dict, key: "sender_img")
            objList.time = Int64(DataChecker .getValidationstring(dict: dict, key: "time"))!
            
            muary.add(objList)
        }
        
        return muary
    }
}
