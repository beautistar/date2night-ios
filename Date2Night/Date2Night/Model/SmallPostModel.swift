//
//  SmallPostModel.swift
//  Date2Night
//
//  Created by Developer on 4/28/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class SmallPostModel {
    
    var id = 0
    var user_id = 0
    var user_photo = ""
    var title = ""
    var description = ""
    var latitude = 0.0
    var longitude = 0.0
    var photo_url = ""
    var video_url = ""
    var reported_count = 0
    var highlighted = ""
    var created_at = ""
}
