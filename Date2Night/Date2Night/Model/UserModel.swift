//
//  UserModel.swift
//  Pixil
//
//  Created by Developer on 8/19/19.
//  Copyright © 2019 Ankhbayar Batbaatar. All rights reserved.
//

import Foundation

class UserModel {
    
    var id: Int = 0
    var name = ""
    var fb_id = 0
    var phone = ""
    var type = ""
    var photo_url = ""
    var photo_url1 = ""
    var photo_url2 = ""
    var age = 0
    var gender = ""
    var orientation = ""
    var instagram = ""
    var snapchat = ""
    var noti_count = 0
    var membership_type = -1
    var membership_date = ""
    var block = 0
    
    
    var token: String {
        get {
            if let token = UserDefaults.standard.value(forKey: Const.KEY_TOKEN) {
                return token as! String
            }
            return ""
        }
    }
}
